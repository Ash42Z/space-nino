use crate::mission::{seconds_to_get_there, AvailableMissions};
use crate::time::{add_seconds, format_seconds, DisplayTimeInfo, StationTime};
use crate::utils::{
    handle_change_state_button, spawn_button_with_component,
    spawn_special_button_with_component, ChangeStateButton,
};
use crate::{
    change_scene, BackgroundSprites, ButtonImage, CycleButtonImages, GameFont,
    GameView, Scene, Ui, UiBox,
};

use bevy::prelude::*;
use iyes_loopless::prelude::*;

const VISIBLE_OPTIONS: f32 = 4.;

pub struct MissionSelectPlugin;

#[derive(Resource)]
pub struct MissionSelectIdx(usize);

impl Plugin for MissionSelectPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(setup)
            .add_enter_system(GameView::MissionSelect, enter)
            .add_system_set(
                ConditionSet::new()
                    .run_in_state(GameView::MissionSelect)
                    .with_system(handle_mission_buttons)
                    .with_system(load_ui)
                    .with_system(handle_next_prev)
                    .with_system(handle_change_state_button)
                    .into(),
            );
    }
}

fn setup(mut commands: Commands) {
    commands.insert_resource(MissionSelectIdx(0));
}

fn enter(mut commands: Commands) {
    commands.insert_resource(MissionSelectIdx(0));
}

#[derive(Component)]
struct ChosenMissionButton(usize);

#[derive(Component)]
struct NextMissionButton(isize);

fn load_ui(
    mut commands: Commands,
    ui: Query<Entity, With<Ui>>,
    box_style: Query<&Style, With<UiBox>>,
    avail: Res<AvailableMissions>,
    mission_idx: Res<MissionSelectIdx>,
    font: Res<GameFont>,
    button_image: Res<ButtonImage>,
    cycle_button_images: Res<CycleButtonImages>,
    mut scene: Query<(Entity, &mut Handle<Image>), With<Scene>>,
    backgrounds: Res<BackgroundSprites>,
    display_time_info: Res<DisplayTimeInfo>,
) {
    if !mission_idx.is_changed() {
        return;
    }

    let Ok((scene, mut image)) = scene.get_single_mut() else { return; };
    change_scene(&mut commands, scene, &mut image, &backgrounds.mission);

    let Ok(ui) = ui.get_single() else { return; };
    commands.entity(ui).despawn_descendants();

    let Ok(Val::Px(width)) = box_style
        .get_single()
        .map(|style| style.size.width)
        else { return; };

    let mission_box = commands
        .spawn(NodeBundle {
            style: Style {
                size: Size {
                    height: Val::Px(width),
                    ..default()
                },
                display: Display::Flex,
                flex_direction: FlexDirection::Column,
                justify_content: JustifyContent::SpaceBetween,
                ..default()
            },
            ..default()
        })
        .id();

    let detail_box = commands
        .spawn(NodeBundle {
            style: Style {
                flex_direction: FlexDirection::Row,
                align_items: AlignItems::FlexStart,
                ..default()
            },
            ..default()
        })
        .id();

    let option_box = commands
        .spawn(NodeBundle {
            style: Style {
                flex_direction: FlexDirection::Column,
                size: Size {
                    height: Val::Px(VISIBLE_OPTIONS * 0.125 * width),
                    ..default()
                },
                ..default()
            },
            ..default()
        })
        .id();

    let mut option_children = vec![];

    if mission_idx.0 < avail.0.len() {
        let mission = &avail.0[mission_idx.0].0;
        let travel_time = seconds_to_get_there(mission.multiplier(), 1);

        let mut text = format!(
            "{}\nTime until launch window: {}",
            mission.name(),
            format_seconds(mission.launch_time()),
        );

        if display_time_info.0 {
            text = format!(
                "{}\n\
                Time difference: {} / second\n\
                Travel: {} ({} on station)\n\
                Mission: {} ({} on station)",
                text,
                format_seconds(mission.multiplier()),
                format_seconds(travel_time),
                format_seconds(travel_time * mission.multiplier()),
                format_seconds(mission.mission_time()),
                format_seconds(mission.mission_time() * mission.multiplier()),
            );
        }

        let detail_children = vec![
            spawn_special_button_with_component(
                &mut commands,
                Style {
                    size: Size {
                        height: Val::Px(23. * width / 128.),
                        width: Val::Px(13. * width / 128.),
                    },
                    ..default()
                },
                &cycle_button_images.prev,
                width,
                NextMissionButton(-1),
            ),
            spawn_special_button_with_component(
                &mut commands,
                Style {
                    size: Size {
                        height: Val::Px(23. * width / 128.),
                        width: Val::Px(14. * width / 128.),
                    },
                    ..default()
                },
                &cycle_button_images.next,
                width,
                NextMissionButton(1),
            ),
            commands
                .spawn(TextBundle {
                    text: Text::from_section(
                        text,
                        TextStyle {
                            font: font.0.clone(),
                            color: Color::AZURE,
                            font_size: width * 0.054,
                        },
                    ),
                    style: Style {
                        margin: UiRect::left(Val::Percent(2.6)),
                        max_size: Size {
                            width: Val::Px(width - (32. * (width / 128.))),
                            ..default()
                        },
                        ..default()
                    },
                    ..default()
                })
                .id(),
        ];

        option_children.push(spawn_button_with_component(
            &mut commands,
            "Go",
            &font.0,
            &button_image.0,
            width,
            ChosenMissionButton(mission_idx.0),
        ));

        commands.entity(detail_box).push_children(&detail_children);
    }

    option_children.push(spawn_button_with_component(
        &mut commands,
        "Back to station",
        &font.0,
        &button_image.0,
        width,
        ChangeStateButton(GameView::Spacestation),
    ));

    commands.entity(option_box).push_children(&option_children);

    let mission_children = vec![detail_box, option_box];
    commands
        .entity(mission_box)
        .push_children(&mission_children);
    commands.entity(ui).add_child(mission_box);
}

fn handle_mission_buttons(
    mut commands: Commands,
    interactions: Query<
        (&Interaction, &ChosenMissionButton),
        Changed<Interaction>,
    >,
    avail: Res<AvailableMissions>,
    mut time: ResMut<StationTime>,
) {
    for (i, button) in interactions.iter() {
        if !matches!(i, Interaction::Clicked) {
            continue;
        }
        let (mission, encounter) = avail.0[button.0].clone();

        add_seconds(&mut time, mission.launch_time() as f64);
        commands
            .insert_resource(NextState(GameView::Descend(mission, encounter)));
    }
}

fn handle_next_prev(
    interactions: Query<
        (&Interaction, &NextMissionButton),
        Changed<Interaction>,
    >,
    avail: Res<AvailableMissions>,
    mut idx: ResMut<MissionSelectIdx>,
) {
    for (i, button) in interactions.iter() {
        if !matches!(i, Interaction::Clicked) {
            continue;
        }

        let mut new_idx = (idx.0 as isize) + button.0;
        if new_idx < 0 {
            new_idx = std::cmp::max(0, (avail.0.len() as isize) - 1);
        } else if new_idx >= avail.0.len() as isize {
            new_idx = 0;
        }

        idx.0 = new_idx as usize;
    }
}
