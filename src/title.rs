use crate::utils::{
    handle_change_state_button, spawn_button_with_component, ChangeStateButton,
};
use crate::{
    change_scene, BackgroundSprites, ButtonImage, GameFont, GameView, Scene,
    Ui, UiBox,
};

use bevy::prelude::*;
use iyes_loopless::prelude::*;

pub struct TitleScreenPlugin;

impl Plugin for TitleScreenPlugin {
    fn build(&self, app: &mut App) {
        app.add_enter_system(GameView::TitleScreen, load_title)
            .add_system_set(
                ConditionSet::new()
                    .run_in_state(GameView::TitleScreen)
                    .with_system(handle_change_state_button)
                    .into(),
            );
    }
}

fn load_title(
    mut commands: Commands,
    ui: Query<Entity, With<Ui>>,
    box_style: Query<&Style, With<UiBox>>,
    font: Res<GameFont>,
    button_image: Res<ButtonImage>,
    backgrounds: Res<BackgroundSprites>,
    mut scene: Query<(Entity, &mut Handle<Image>), With<Scene>>,
) {
    let (scene, mut image) = scene.get_single_mut().unwrap();
    change_scene(&mut commands, scene, &mut image, &backgrounds.title);

    let ui = ui.get_single().unwrap();
    commands.entity(ui).despawn_descendants();

    let width = if let Some(Val::Px(width)) =
        box_style
            .get_single()
            .ok()
            .map(|style| style.size.width)
    {
        width
    } else {
        400.
    };

    let children = vec![
        commands
            .spawn(TextBundle {
                text: Text::from_section(
                    "Event Horizon",
                    TextStyle {
                        color: Color::WHITE,
                        font: font.0.clone(),
                        font_size: width * 0.22,
                    },
                ),
                style: Style {
                    margin: UiRect {
                        top: Val::Percent(8.),
                        bottom: Val::Percent(14.),
                        ..default()
                    },
                    align_self: AlignSelf::Center,
                    ..default()
                },
                ..default()
            })
            .id(),
        spawn_button_with_component(
            &mut commands,
            "Start".into(),
            &font.0,
            &button_image.0,
            width,
            ChangeStateButton(GameView::Intro),
        ),
    ];

    commands.entity(ui).push_children(&children);
}
