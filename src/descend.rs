use crate::mission::seconds_to_get_there;
use crate::time::{add_seconds, StationTime};
use crate::{GameFont, GameView, Ui, UiBox};
use crate::encounter::CancelMission;

use bevy::prelude::*;
use iyes_loopless::prelude::*;

pub struct DescendPlugin;

impl Plugin for DescendPlugin {
    fn build(&self, app: &mut App) {
        app.add_system(load_ui.run_if(entered_state))
            .add_system(load_next.run_if_resource_exists::<DescendTimer>());
    }
}

#[derive(Resource)]
struct DescendTimer(Timer);

const ANIMATION_TIME: f32 = 3.;

fn load_next(
    mut commands: Commands,
    mut timer: ResMut<DescendTimer>,
    time: Res<Time>,
    state: Res<CurrentState<GameView>>,
    mut station_time: ResMut<StationTime>,
    mut cancel: ResMut<CancelMission>
) {
    if !timer.0.tick(time.delta()).just_finished() {
        return;
    }
    commands.remove_resource::<DescendTimer>();

    let GameView::Descend(mission, encounter) = state.0.clone() else {
        return;
    };

    let old_mult = station_time.multiplier;

    if let Some(encounter) = encounter {
        cancel.0 = false;
        station_time.multiplier = encounter.multiplier();
        add_seconds(
            &mut station_time,
            seconds_to_get_there(encounter.multiplier(), old_mult) as f64,
        );
        encounter.spawn(mission, &mut commands);
    } else {
        station_time.multiplier = mission.multiplier();
        add_seconds(
            &mut station_time,
            seconds_to_get_there(mission.multiplier(), old_mult) as f64,
        );
        mission.spawn(&mut commands);
    }
}

fn load_ui(
    mut commands: Commands,
    ui: Query<Entity, With<Ui>>,
    box_style: Query<&Style, With<UiBox>>,
    font: Res<GameFont>,
    timer: Option<Res<DescendTimer>>,
) {
    let Ok(ui) = ui.get_single() else { return; };
    commands.entity(ui).despawn_descendants();

    let Ok(Val::Px(width)) = box_style
        .get_single()
        .map(|style| style.size.width)
        else { return; };

    let children = vec![commands
        .spawn(TextBundle {
            text: Text::from_section(
                "Descending...",
                TextStyle {
                    color: Color::AZURE,
                    font: font.0.clone(),
                    font_size: width * 0.13,
                },
            ),
            style: Style {
                align_self: AlignSelf::Center,
                margin: UiRect::top(Val::Percent(2.)),
                ..default()
            },
            ..default()
        })
        .id()];

    commands.entity(ui).push_children(&children);
    if timer.is_some() {
        commands.remove_resource::<DescendTimer>();
    }
    commands.insert_resource(DescendTimer(Timer::from_seconds(
        ANIMATION_TIME,
        TimerMode::Once,
    )));
}

fn entered_state(state: Res<CurrentState<GameView>>) -> bool {
    matches!(state.0, GameView::Descend(..)) && state.is_changed()
}
