use crate::gamestate::{CharacterName, Face};
use crate::topic::{ChosenEvent, NextDialogue, Topic, TopicProgress};
use crate::utils::spawn_button_with_component;
use crate::{ButtonImage, GameFont, Ui, UiBox};

use bevy::prelude::*;

use std::collections::VecDeque;
use std::marker::PhantomData;

const VISIBLE_CHOICES: f32 = 4.;

pub struct DialoguePlugin<E> {
    _phantom: PhantomData<E>,
}

impl<E> Default for DialoguePlugin<E> {
    fn default() -> Self {
        Self {
            _phantom: Default::default(),
        }
    }
}

impl<T: Topic> Plugin for DialoguePlugin<T> {
    fn build(&self, app: &mut App) {
        app.add_system(spawn_dialogue::<T>)
            .add_system(advance_dialogue::<T>)
            .add_system(handle_dialogue_buttons::<T>)
            .add_event::<SpawnDialogue<T>>()
            .add_event::<AdvanceDialogue<T>>();
    }
}

pub struct SpawnDialogue<T: Topic> {
    data: DialogueData<T>,
}

impl<T: Topic> SpawnDialogue<T> {
    pub fn new(data: DialogueData<T>) -> Self {
        Self { data }
    }
}

struct AdvanceDialogue<T> {
    _phantom: PhantomData<T>,
}

impl<T> Default for AdvanceDialogue<T> {
    fn default() -> Self {
        Self {
            _phantom: Default::default(),
        }
    }
}

#[derive(Component)]
struct AdvanceButton<T> {
    _phantom: PhantomData<T>,
}

impl<T> Default for AdvanceButton<T> {
    fn default() -> Self {
        Self {
            _phantom: Default::default(),
        }
    }
}

#[derive(Component)]
struct DialogueOption<T: Topic> {
    id: usize,
    next: Option<NextDialogue<T>>,
}

impl<T: Topic> DialogueOption<T> {
    fn new(id: usize, next: Option<NextDialogue<T>>) -> Self {
        Self { id, next }
    }
}

#[derive(Resource, Clone)]
pub struct DialogueData<T: Topic> {
    text: VecDeque<(&'static str, Option<&'static str>)>,
    options: Vec<(&'static str, Option<NextDialogue<T>>)>,
}

impl<T: Topic> DialogueData<T> {
    pub fn new(
        text: Vec<(&'static str, Option<&'static str>)>,
        options: Vec<(&'static str, Option<NextDialogue<T>>)>,
    ) -> Self {
        Self {
            text: text.into(),
            options,
        }
    }
}

fn spawn_dialogue<T: Topic>(
    mut commands: Commands,
    mut spawn_evs: EventReader<SpawnDialogue<T>>,
    mut advance_wr: EventWriter<AdvanceDialogue<T>>,
) {
    if let Some(ev) = spawn_evs.iter().next() {
        commands.insert_resource(DialogueData::<T>::new(
            ev.data.text.clone().into(),
            ev.data.options.clone(),
        ));
        advance_wr.send(AdvanceDialogue::<T>::default());
    }
}

fn advance_dialogue<T: Topic>(
    mut commands: Commands,
    mut advance_evs: EventReader<AdvanceDialogue<T>>,
    data: Option<ResMut<DialogueData<T>>>,
    font: Res<GameFont>,
    button_image: Res<ButtonImage>,
    characters: Query<(&Face, &CharacterName)>,
    ui: Query<Entity, With<Ui>>,
    box_style: Query<&Style, With<UiBox>>,
) {
    let Some(mut data) = data else { return; };

    if advance_evs.iter().next().is_none() {
        return;
    }

    let Ok(ui) = ui.get_single() else {
        return;
    };

    let Ok(Val::Px(width)) = box_style
        .get_single()
        .map(|style| style.size.width)
        else { return; };

    let next_text = data.text.pop_front().unwrap_or(("".into(), None));
    commands.entity(ui).despawn_descendants();

    let content_box = commands.spawn(NodeBundle {
        style: Style {
            flex_direction: FlexDirection::Column,
            size: Size {
                height: Val::Px(width * 0.4),
                ..default()
            },
            margin: UiRect::left(Val::Percent(1.)),
            ..default()
        },
        ..default()
    }).id();

    if let Some(speaker) = next_text.1 {
        let speaker_image = characters
            .iter()
            .find_map(|(image, name)| {
                (name.0 == speaker).then(|| image.0.clone())
            })
            .unwrap_or_default();

        let speaker_box = commands
            .spawn(NodeBundle {
                style: Style {
                    display: Display::Flex,
                    flex_direction: FlexDirection::Row,
                    align_items: AlignItems::Center,
                    ..default()
                },
                ..default()
            })
            .with_children(|b| {
                b.spawn(ImageBundle {
                    image: UiImage(speaker_image),
                    style: Style {
                        size: Size {
                            height: Val::Px(20. * width / 128.),
                            width: Val::Px(20. * width / 128.),
                        },
                        margin: UiRect::all(Val::Percent(0.6)),
                        ..default()
                    },
                    ..default()
                });
                b.spawn(TextBundle {
                    text: Text::from_section(
                        speaker,
                        TextStyle {
                            color: Color::GRAY,
                            font: font.0.clone(),
                            font_size: width * 0.08,
                        },
                    ),
                    style: Style {
                        margin: UiRect::top(Val::Percent(6.)),
                        ..default()
                    },
                    ..default()
                });
            })
            .id();

        commands.entity(content_box).add_child(speaker_box);
    }

    let text = commands
        .spawn(TextBundle {
            text: Text::from_section(
                next_text.0,
                TextStyle {
                    color: Color::AZURE,
                    font: font.0.clone(),
                    font_size: width * 0.07,
                },
            ),
            style: Style {
                max_size: Size {
                    width: Val::Px(width * 0.96),
                    ..default()
                },
                margin: UiRect::all(Val::Percent(1.)),
                ..default()
            },
            ..default()
        })
        .id();
    commands.entity(content_box).add_child(text);

    let children = if data.text.is_empty() {
        data.options
            .iter()
            .enumerate()
            .map(|(id, option)| {
                spawn_button_with_component(
                    &mut commands,
                    option.0,
                    &font.0,
                    &button_image.0,
                    width,
                    DialogueOption::<T>::new(id, option.1.clone()),
                )
            })
            .collect()
    } else {
        vec![spawn_button_with_component(
            &mut commands,
            "Next".into(),
            &font.0,
            &button_image.0,
            width,
            AdvanceButton::<T>::default(),
        )]
    };

    let mut button_box = commands.spawn(NodeBundle {
        style: Style {
            flex_direction: FlexDirection::Column,
            size: Size {
                height: Val::Px(VISIBLE_CHOICES * 0.125 * width),
                ..default()
            },
            ..default()
        },
        ..default()
    });

    button_box.push_children(&children);

    let dialogue_box_children = vec![content_box, button_box.id()];
    let mut dialogue_box = commands.spawn(NodeBundle {
        style: Style {
            size: Size {
                height: Val::Px(width),
                ..default()
            },
            display: Display::Flex,
            flex_direction: FlexDirection::Column,
            justify_content: JustifyContent::SpaceBetween,
            ..default()
        },
        ..default()
    });

    dialogue_box.push_children(&dialogue_box_children);
    let dialogue_box_id = dialogue_box.id();
    commands.entity(ui).add_child(dialogue_box_id);
}

fn handle_dialogue_buttons<T: Topic>(
    mut advance_wr: EventWriter<AdvanceDialogue<T>>,
    mut option_wr: EventWriter<ChosenEvent<T>>,
    next: Query<&Interaction, (Changed<Interaction>, With<AdvanceButton<T>>)>,
    options: Query<(&Interaction, &DialogueOption<T>), Changed<Interaction>>,
    mut next_wr: EventWriter<NextDialogue<T>>,
    ui: Query<Entity, With<Ui>>,
    current: Option<Res<TopicProgress<T>>>,
    mut commands: Commands,
) {
    if let Ok(ui) = ui.get_single() {
        for interaction in next.iter() {
            if matches!(interaction, Interaction::Clicked) {
                advance_wr.send(AdvanceDialogue::default());
                return;
            } 
        }

        for (interaction, option) in options.iter() {
            if matches!(interaction, Interaction::Clicked) {
                commands.entity(ui).despawn_descendants();
                commands.remove_resource::<DialogueData<T>>();
                if let Some(current) = &current {
                    option_wr
                        .send(ChosenEvent::new(current.0.clone(), option.id));
                }
                if let Some(next) = &option.next {
                    next_wr.send(next.clone());
                }
                return;
            }
        }
    }
}
