use crate::gamestate::{minimum_fuel, success_rate, Fuel, NavData};
use crate::utils::{
    handle_change_state_button, spawn_button_with_component, ChangeStateButton,
};
use crate::{
    change_scene, BackgroundSprites, ButtonImage, GameFont, GameView, Scene,
    Ui, UiBox,
};

use bevy::prelude::*;
use iyes_loopless::prelude::*;

const VISIBLE_OPTIONS: f32 = 2.;
pub struct CockpitPlugin;

impl Plugin for CockpitPlugin {
    fn build(&self, app: &mut App) {
        app.add_enter_system(GameView::Cockpit, load_cockpit)
            .add_system_set(
                ConditionSet::new()
                    .run_in_state(GameView::Cockpit)
                    .with_system(handle_change_state_button)
                    .with_system(handle_text)
                    .into(),
            );
    }
}

#[derive(Component)]
struct FuelText;

#[derive(Component)]
struct NavText;

#[derive(Component)]
struct SuccessText;

fn handle_text(
    mut fuel_text: Query<
        &mut Text,
        (With<FuelText>, Without<NavText>, Without<SuccessText>),
    >,
    mut nav_text: Query<
        &mut Text,
        (With<NavText>, Without<FuelText>, Without<SuccessText>),
    >,
    mut success_text: Query<
        &mut Text,
        (With<SuccessText>, Without<FuelText>, Without<NavText>),
    >,
    fuel: Res<Fuel>,
    nav: Res<NavData>,
    font: Res<GameFont>,
) {
    if let Ok(mut text) = fuel_text.get_single_mut() {
        *text = get_text(
            format!("Fuel cells: {} (Minimum {})", fuel.0, minimum_fuel(nav.0)),
            font.0.clone(),
        );
    }

    if let Ok(mut text) = nav_text.get_single_mut() {
        *text = get_text(
            format!("Navigational data: {}", nav.format()),
            font.0.clone(),
        );
    }

    if let Ok(mut text) = success_text.get_single_mut() {
        *text = get_text(
            format!("Success rate: {}%", success_rate(fuel.0, nav.0)),
            font.0.clone(),
        );
    }
}

fn get_text(s: String, font: Handle<Font>) -> Text {
    Text::from_section(
        s,
        TextStyle {
            color: Color::AZURE,
            font,
            font_size: 50.,
        },
    )
}

fn load_cockpit(
    mut commands: Commands,
    font: Res<GameFont>,
    button_image: Res<ButtonImage>,
    ui: Query<Entity, With<Ui>>,
    box_style: Query<&Style, With<UiBox>>,
    mut scene: Query<(Entity, &mut Handle<Image>), With<Scene>>,
    backgrounds: Res<BackgroundSprites>,
    fuel: Res<Fuel>,
    nav: Res<NavData>,
) {
    let Ok((scene, mut image)) = scene.get_single_mut() else { return; };
    change_scene(&mut commands, scene, &mut image, &backgrounds.cockpit);

    let Ok(ui) = ui.get_single() else { return; } ;
    commands.entity(ui).despawn_descendants();

    let Ok(Val::Px(width)) = box_style
        .get_single()
        .map(|style| style.size.width)
        else { return; };

    // TODO: not copy this for every file.
    let cockpit_box = commands
        .spawn(NodeBundle {
            style: Style {
                size: Size {
                    height: Val::Px(width),
                    ..default()
                },
                display: Display::Flex,
                flex_direction: FlexDirection::Column,
                justify_content: JustifyContent::SpaceBetween,
                ..default()
            },
            ..default()
        })
        .id();

    let content_box = commands
        .spawn(NodeBundle {
            style: Style {
                flex_direction: FlexDirection::Column,
                align_items: AlignItems::FlexStart,
                margin: UiRect::horizontal(Val::Percent(2.)),
                ..default()
            },
            ..default()
        })
        .id();

    let option_box = commands
        .spawn(NodeBundle {
            style: Style {
                flex_direction: FlexDirection::Column,
                justify_content: JustifyContent::FlexEnd,
                size: Size {
                    height: Val::Px(VISIBLE_OPTIONS * 0.125 * width),
                    ..default()
                },
                ..default()
            },
            ..default()
        })
        .id();

    let content_children = vec![
        commands
            .spawn(NodeBundle {
                style: Style {
                    flex_direction: FlexDirection::Row,
                    justify_content: JustifyContent::SpaceBetween,
                    size: Size {
                        width: Val::Px(width * 0.8),
                        ..default()
                    },
                    ..default()
                },
                ..default()
            })
            .with_children(|row| {
                row.spawn((
                    FuelText,
                    TextBundle {
                        text: get_text("".into(), font.0.clone()),
                        ..default()
                    },
                ));
            })
            .id(),
        commands
            .spawn(NodeBundle {
                style: Style {
                    flex_direction: FlexDirection::Row,
                    justify_content: JustifyContent::SpaceBetween,
                    size: Size {
                        width: Val::Px(width * 0.8),
                        ..default()
                    },
                    ..default()
                },
                ..default()
            })
            .with_children(|row| {
                row.spawn((
                    NavText,
                    TextBundle {
                        text: get_text("".into(), font.0.clone()),
                        ..default()
                    },
                ));
            })
            .id(),
        commands
            .spawn(NodeBundle {
                style: Style {
                    flex_direction: FlexDirection::Row,
                    justify_content: JustifyContent::SpaceBetween,
                    size: Size {
                        width: Val::Px(width * 0.8),
                        ..default()
                    },
                    ..default()
                },
                ..default()
            })
            .with_children(|row| {
                row.spawn((
                    SuccessText,
                    TextBundle {
                        text: get_text("".into(), font.0.clone()),
                        ..default()
                    },
                ));
            })
            .id(),
    ];
    commands
        .entity(content_box)
        .push_children(&content_children);

    let mut option_children = vec![];

    if success_rate(fuel.0, nav.0) > 0 {
        option_children.push(spawn_button_with_component(
            &mut commands,
            "Launch".into(),
            &font.0,
            &button_image.0,
            width,
            ChangeStateButton(GameView::Launch),
        ));
    }

    option_children.push(spawn_button_with_component(
        &mut commands,
        "Back".into(),
        &font.0,
        &button_image.0,
        width,
        ChangeStateButton(GameView::Spacestation),
    ));

    commands.entity(option_box).push_children(&option_children);

    commands
        .entity(cockpit_box)
        .push_children(&vec![content_box, option_box]);
    commands.entity(ui).add_child(cockpit_box);
}
