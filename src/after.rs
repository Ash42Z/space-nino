use crate::encounter::{CancelMission, CurrentEncounter};
use crate::gamestate::{Character, ConversationIndex, MissionCount};
use crate::mission::{
    seconds_to_get_there, AvailableEncounters, CurrentMission,
};
use crate::shared_topics::{spawn_topic_on_return, SharedTopicsState};
use crate::time::{add_seconds, StationTime};
use crate::{BackgroundSprites, GameFont, GameView, Scene, Ui, UiBox};

use bevy::prelude::*;
use iyes_loopless::prelude::*;

pub struct AfterPlugin;

impl Plugin for AfterPlugin {
    fn build(&self, app: &mut App) {
        app.add_enter_system(GameView::AfterMission, after_mission)
            .add_system(after_encounter)
            .add_system(
                handle_ascend_timer
                    .run_in_state(GameView::AfterMission)
                    .run_if_resource_exists::<AscendTimer>(),
            );
    }
}

fn after_encounter(
    mut commands: Commands,
    state: Res<CurrentState<GameView>>,
    mut cancel: ResMut<CancelMission>,
    mut encounters: ResMut<AvailableEncounters>,
) {
    if let GameView::AfterEncounter(mission) = &state.0 {
        let did_cancel = cancel.0;
        cancel.0 = false;
        commands.remove_resource::<CurrentEncounter>();
        encounters.0.remove(0);

        if did_cancel {
            commands.insert_resource(NextState(GameView::AfterMission));
        } else {
            commands.insert_resource(NextState(GameView::Descend(
                mission.clone(),
                None,
            )));
        }
    }
}

#[derive(Resource)]
struct AscendTimer(Timer);

const ASCEND_TIME: f32 = 3.;

fn after_mission(
    mut commands: Commands,
    ui: Query<Entity, With<Ui>>,
    box_style: Query<&Style, With<UiBox>>,
    font: Res<GameFont>,
) {
    commands.remove_resource::<CurrentMission>();
    commands.insert_resource(AscendTimer(Timer::from_seconds(
        ASCEND_TIME,
        TimerMode::Once,
    )));

    if let Ok(ui) = ui.get_single() {
        commands.entity(ui).despawn_descendants();

        let Ok(Val::Px(width)) = box_style
            .get_single()
            .map(|style| style.size.width)
            else { return; };

        let child = commands
            .spawn(TextBundle {
                text: Text::from_section(
                    "Ascending...",
                    TextStyle {
                        color: Color::AZURE,
                        font: font.0.clone(),
                        font_size: width * 0.13,
                    },
                ),
                style: Style {
                    align_self: AlignSelf::Center,
                    margin: UiRect::top(Val::Percent(2.)),
                    ..default()
                },
                ..default()
            })
            .id();

        commands.entity(ui).add_child(child);
    }
}

fn handle_ascend_timer(
    mut commands: Commands,
    mut timer: ResMut<AscendTimer>,
    time: Res<Time>,
    state: Res<SharedTopicsState>,
    backgrounds: Res<BackgroundSprites>,
    characters: Query<&Handle<Image>, With<Character>>,
    mut scene: Query<
        (Entity, &mut Handle<Image>),
        (With<Scene>, Without<Character>, Without<Ui>),
    >,
    mut station_time: ResMut<StationTime>,
    mut mission_count: ResMut<MissionCount>,
    mut conversation_idx: Query<&mut ConversationIndex>,
) {
    if timer.0.tick(time.delta()).just_finished() {
        let Ok((scene, mut image)) = scene.get_single_mut() else { return; };

        commands.remove_resource::<AscendTimer>();
        let mult = station_time.multiplier;
        add_seconds(&mut station_time, seconds_to_get_there(1, mult) as f64);
        station_time.multiplier = 1;

        mission_count.0 += 1;

        for mut idx in conversation_idx.iter_mut() {
            idx.0 = 0;
        }

        if !spawn_topic_on_return(
            &mut commands,
            &state,
            mission_count.0,
            &backgrounds,
            characters.iter().cloned().collect(),
            (scene, &mut image),
        ) {
            commands.insert_resource(NextState(GameView::Spacestation));
        }
    }
}
