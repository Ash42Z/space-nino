use crate::gamestate::{success_rate, Fuel, NavData};
use crate::{GameFont, GameView, Ui, UiBox};

use bevy::prelude::*;
use iyes_loopless::prelude::*;
use rand::Rng;

pub struct EndPlugin;

impl Plugin for EndPlugin {
    fn build(&self, app: &mut App) {
        app.add_enter_system(GameView::End, on_end);
    }
}

fn on_end(
    mut commands: Commands,
    fuel: Res<Fuel>,
    nav: Res<NavData>,
    ui: Query<Entity, With<Ui>>,
    box_style: Query<&Style, With<UiBox>>,
    font: Res<GameFont>,
) {
    let mut rng = rand::thread_rng();
    let rand: f64 = rng.gen();

    let text = if success_rate(fuel.0, nav.0) > (rand * 100.) as u32 {
        "The journey back to Earth was long and hard, but after a very long time \
        in space, you have managed to bring everyone back to safety."
    } else {
        "You tried your best to pilot the ship back to Earth. As the last \
        fuel cell is spent, you are left to drift in deep space forever."
    };

    let Ok(ui) = ui.get_single() else { return; };
    commands.entity(ui).despawn_descendants();

    let Ok(Val::Px(width)) = box_style
        .get_single()
        .map(|style| style.size.width)
        else { return; };

    let children = vec![
        commands
            .spawn(TextBundle {
                text: Text::from_section(
                    text,
                    TextStyle {
                        color: Color::AZURE,
                        font: font.0.clone(),
                        font_size: width * 0.12,
                    },
                ),
                style: Style {
                    max_size: Size {
                        width: Val::Px(width * 0.96),
                        ..default()
                    },
                    margin: UiRect {
                        top: Val::Percent(2.),
                        left: Val::Percent(2.),
                        ..default()
                    },
                    ..default()
                },
                ..default()
            })
            .id(),
        commands
            .spawn(TextBundle {
                text: Text::from_section(
                    "THE END",
                    TextStyle {
                        color: Color::AZURE,
                        font: font.0.clone(),
                        font_size: width * 0.15,
                    },
                ),
                style: Style {
                    align_self: AlignSelf::Center,
                    max_size: Size {
                        width: Val::Px(width * 0.96),
                        ..default()
                    },
                    margin: UiRect {
                        top: Val::Percent(2.),
                        left: Val::Percent(2.),
                        ..default()
                    },
                    ..default()
                },
                ..default()
            })
            .id(),
    ];

    commands.entity(ui).push_children(&children);
}
