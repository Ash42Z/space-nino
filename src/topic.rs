use crate::dialogue::{DialogueData, DialoguePlugin, SpawnDialogue};
use crate::GameView;

use bevy::prelude::*;
use iyes_loopless::prelude::*;

use std::marker::PhantomData;

pub trait Topic: Sized + Default + Send + Sync + 'static + Clone {
    fn data(&self) -> DialogueData<Self>;

    fn spawn(commands: &mut Commands, view: GameView) {
        let start = Self::default();
        let start_clone = start.clone();
        commands.add(move |w: &mut World| {
            let mut events_resource = w.resource_mut::<Events<_>>();
            events_resource.send(SpawnDialogue::new(start.data()));
        });
        commands.insert_resource(TopicProgress(start_clone));
        commands.insert_resource(NextState(GameView::Topic(Box::new(view))));
    }
}

pub struct TopicPlugin<T> {
    _phantom: PhantomData<T>,
}

impl<T> Default for TopicPlugin<T> {
    fn default() -> Self {
        Self {
            _phantom: Default::default(),
        }
    }
}

#[derive(Resource)]
pub struct TopicProgress<T: Topic>(pub T);

pub struct ChosenEvent<T: Topic> {
    pub current: T,
    pub chosen: usize,
}

impl<T: Topic> ChosenEvent<T> {
    pub fn new(current: T, chosen: usize) -> Self {
        Self { current, chosen }
    }
}

#[derive(Clone)]
pub enum NextDialogue<T: Topic> {
    Next(T),
    End,
}

impl<T: Topic + Send + Sync + 'static> Plugin for TopicPlugin<T> {
    fn build(&self, app: &mut App) {
        app.add_plugin(DialoguePlugin::<T>::default())
            .add_system(Self::handle_next_dialogue)
            .add_event::<ChosenEvent<T>>()
            .add_event::<NextDialogue<T>>();
    }
}

impl<T: Topic + Send + Sync + 'static> TopicPlugin<T> {
    fn handle_next_dialogue(
        mut next_evs: EventReader<NextDialogue<T>>,
        mut spawn_wr: EventWriter<SpawnDialogue<T>>,
        mut progress: Option<ResMut<TopicProgress<T>>>,
        mut commands: Commands,
        state: Res<CurrentState<GameView>>,
    ) {
        for ev in next_evs.iter() {
            match ev {
                NextDialogue::Next(t) => {
                    spawn_wr.send(SpawnDialogue::new(t.data()));
                    progress.as_mut().unwrap().0 = t.clone();
                }
                NextDialogue::End => {
                    commands.remove_resource::<TopicProgress<T>>();
                    if let GameView::Topic(prev_state) = &state.0 {
                        let prev_state: GameView = *prev_state.clone();
                        commands.insert_resource(NextState(prev_state));
                    }
                }
            }
        }
    }
}
