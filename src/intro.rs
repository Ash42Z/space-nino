use crate::dialogue::DialogueData;
use crate::gamestate::Character;
use crate::topic::{ChosenEvent, NextDialogue, Topic, TopicPlugin};
use crate::utils::spawn_lounge_scene;
use crate::{change_scene, BackgroundSprites, GameView, Scene, Ui};

use bevy::prelude::*;
use iyes_loopless::prelude::*;

pub struct IntroPlugin;

impl Plugin for IntroPlugin {
    fn build(&self, app: &mut App) {
        app.add_enter_system(GameView::Intro, load_intro)
            .add_plugin(TopicPlugin::<Intro>::default())
            .add_system(handle_intro);
    }
}

fn load_intro(
    mut commands: Commands,
    backgrounds: Res<BackgroundSprites>,
    mut scene: Query<(Entity, &mut Handle<Image>), With<Scene>>,
    ui: Query<Entity, With<Ui>>,
) {
    let Ok(ui) = ui.get_single() else { return; };
    commands.entity(ui).despawn_descendants();

    let (scene, mut image) = scene.get_single_mut().unwrap();
    change_scene(&mut commands, scene, &mut image, &backgrounds.cockpit);

    Intro::spawn(&mut commands, GameView::Spacestation);
}

#[derive(Clone, Debug)]
enum Intro {
    Start,
    LoungeStart,
    Umm,
    CalmDown,
    Chaos,
    Rebooting,
    Solution
}

impl Default for Intro {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for Intro {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => {
                DialogueData::new(
                    vec![
                        ("[REACHING DESTINATION IN 4 MINUTES]", None),
                        ("[BLACK HOLE GRAVITATIONAL FIELD DETECTED]", None),
                        ("Whoa guys, look out the window!", Some("Nora")),
                        ("I sorta feel it pulling me closer.", Some("Nicholas")),
                        ("[ORBIT STABILIZING]", None),
                        ("* TSSSSS *", None),
                        ("This trip - Totally worth it.", Some("Patricia")),
                        ("I'll admit it. It's breathtaking.", Some("Stanley")),
                        ("* BOOM *", None),
                        ("[WARNING]", None),
                        ("* BOOM! *", None),
                        ("[WARNING!!!]", None),
                        ("[ENGINE MALFUNCTIONING]", None),
                        ("Guys, what's going on?", Some("Nora")),
                        ("[TURNING ON BACKUP ENGINE]", None),
                        ("[...]", None),
                        ("[......]", None),
                        ("[BACKUP ENGINE MALFUNCTIONING]", None),
                        ("[FUEL TANK MALFUNCTIONING]", None),
                        ("[ATTEMPTING EMERGENCY FUEL REPLACEMENT]", None),
                        ("[CRITICAL ERROR]", None),
                        ("[FUEL TANK EMPTY]", None),
                        ("[SHIP ENTERING EMERGENCY MODE]", None),
                        ("...", None),
                        ("Excuse me? Pilot?", Some("Nora")),
                        ("What's going on?", Some("Nora")),
                    ],
                    vec![(
                        "[Go meet everyone at the lounge]",
                        Some(NextDialogue::Next(Self::LoungeStart))
                    )]
                )
            },
            Self::LoungeStart => {
                DialogueData::new(
                    vec![
                        ("...", None),
                        ("So?", Some("Nora")),
                        ("What's going on?", Some("Stanley")),
                    ], 
                    vec![
                        (
                            "Um...",
                            Some(NextDialogue::Next(Self::Umm))
                        ),
                        (
                            "I'm not sure.",
                            Some(NextDialogue::Next(Self::Umm))
                        ),
                        (
                            "Uhhhhh.",
                            Some(NextDialogue::Next(Self::Umm))
                        )
                    ]
                )
            },
            Self::Umm => {
                DialogueData::new(
                    vec![
                        ("OMG. Is this it??", Some("Nora")),
                        ("Are we gonna die????", Some("Nora")),
                        ("I knew I never should have come here.", Some("Stanley")),
                        ("And the black hole looks so pretty from here.", Some("Patricia")),
                        ("Oh no oh no...", Some("Nora")),
                        ("The black hole...", Some("Nora")),
                        ("NO NO NO NO NO", Some("Nora")),
                        ("ARE WE GONNA FALL IN??", Some("Nora")),
                    ], 
                    vec![
                        ("[Try calm them down]", Some(NextDialogue::Next(Self::CalmDown))),
                        ("[Stand in shock as the chaos continues]", Some(NextDialogue::Next(Self::Chaos))),
                    ]
                )
            },
            Self::CalmDown => {
                DialogueData::new(
                    vec![
                        ("Calm??", Some("Stanley")),
                        ("How do you expect us to be calm??!", Some("Stanley")),
                        ("Look at this.", Some("Stanley")),
                        ("Look out the window.", Some("Stanley")),
                        ("You brought us to this monstrosity.", Some("Stanley")),
                        ("Now we're gonna get sucked in.", Some("Stanley")),
                        ("It's only a matter of time.", Some("Stanley"))
                    ], 
                    vec![
                        ("[Promise to figure something out]", Some(NextDialogue::Next(Self::Rebooting))),
                        ("Just... Give me a minute.", Some(NextDialogue::Next(Self::Rebooting)))
                    ]
                )
            },
            Self::Chaos => {
                DialogueData::new(
                    vec![
                        ("WE'RE ALL GONNA DIE", Some("Nora")),
                        ("We were always going to die.", Some("Stanley")),
                        ("This sucks.", Some("Nicholas")),
                        ("Maybe I could have a look at the engines?", Some("Patricia")),
                        ("I am an astrophysicist, after all.", Some("Patricia")),
                    ], 
                    vec![
                        ("[Promise to figure something out]",  Some(NextDialogue::Next(Self::Rebooting))),
                        ("Just... Give me a minute.",  Some(NextDialogue::Next(Self::Rebooting)))
                    ]
                )
            },
            Self::Rebooting => {
                DialogueData::new(
                    vec![("So what do we do?", Some("Nicholas")),
                    ("[INITIATING AUTOMATIC TROUBLESHOOTING PROTOCOL]", None),
                    ("[...]", None),
                    ("[Testing Ship Trajectory...]", None),
                    ("[Testing Ship Trajectory...COMPLETE]", None),
                    ("[Ship Trajectory Around Gravitational Object....STABLE]", None),
                    ("[...]", None),
                    ("[Testing For Damage...]", None),
                    ("[Testing For Damage...COMPLETE]", None),
                    ("[No Major Damage Found.]", None),
                    ("[...]", None),
                    ("[Testing Travel Parameters...]", None),
                    ("[Testing Travel Parameters...COMPLETE]", None),
                    ("[Two Problems Found]", None),
                    ("[Fuel Tank Is Empty]", None),
                    ("[Navigational Data For Earth Lost]", None),
                    ("[Two Solutions Found]", None),
                    ("[Fuel Cells Can Be Synthesized From Plutonium]", None),
                    ("[Navigational Data Can Be Measured From Strategic Locations]", None),
                    ("[Searching For Resources...]", None),
                    ("[...]", None),
                    ("[.....]", None),
                    ("[Locations Found]", None),
                    ("[Nearby Asteroids With Plutonium Located]", None),
                    ("[Coordinates Sent To Landing Pod]", None),
                    ("[...]", None),
                    ("[.....]", None),
                    ("[TROUBLESHOOTING PROTOCOL COMPLETED]", None),
                    ("...", None),
                        ],
                    vec![
                        ("...", Some(NextDialogue::Next(Self::Solution))),
                        ("...", Some(NextDialogue::Next(Self::Solution))),
                        ("So... that's one way to solve the problem.", Some(NextDialogue::Next(Self::Solution)))
                    ]
                )
            },
            Self::Solution => {
                DialogueData::new(
                    vec![
                        ("...", None),
                        ("Well...", Some("Nora")),
                        ("Well, at least we know we'll be fine. Right?", Some("Patricia")),
                        ("Eh...", Some("Nora")),
                        ("Let's wait and see what happens.", Some("Nora")),
                        ("...", None),
                        ("......", None),
                        ("So??", Some("Nora")),
                        ("Are you gonna fix the problem, or what?", Some("Nora"))
                    ], 
                    vec![("I'll check it out.", Some(NextDialogue::End)),
                    ("Don't worry, I'm on it.", Some(NextDialogue::End)),
                    ]
                )
            },
        }
    }
}

fn handle_intro(
    mut commands: Commands,
    backgrounds: Res<BackgroundSprites>,
    mut scene: Query<(Entity, &mut Handle<Image>), With<Scene>>,
    mut option_evs: EventReader<ChosenEvent<Intro>>,
    characters: Query<&Handle<Image>, (With<Character>, Without<Scene>)>,
) {
    for ev in option_evs.iter() {
        let (scene, mut background) = scene.get_single_mut().unwrap();

        match ev.current {
            Intro::Start => {
                spawn_lounge_scene(
                    &mut commands,
                    &backgrounds,
                    characters.iter().cloned().collect(),
                    (scene, &mut background),
                );
            }
            _ => {}
        }
    }
}
