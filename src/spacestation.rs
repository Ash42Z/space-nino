use crate::gamestate::{Alive, Character, CharacterName, RequestTopicEvent};
use crate::utils::{
    handle_change_state_button, spawn_button_with_component,
    spawn_lounge_scene, ChangeStateButton, spawn_special_button_with_component,
};
use crate::{BackgroundSprites, ButtonImage, GameFont, GameView, Scene, Ui, UiBox, CycleButtonImages};

use crate::nicholas::Nicholas;
use crate::nora::Nora;
use crate::patricia::Patricia;
use crate::stanley::Stanley;

use bevy::prelude::*;
use iyes_loopless::prelude::*;

const VISIBLE_OPTIONS: f32 = 4.;

pub struct SpaceStationPlugin;

#[derive(Component)]
struct Active;

#[derive(Component)]
struct CharacterNameUi;

impl Plugin for SpaceStationPlugin {
    fn build(&self, app: &mut App) {
        app.add_enter_system(GameView::Spacestation, load_ship_ui)
            .add_enter_system(GameView::Spacestation, init_station_state)
            .add_system_set(
                ConditionSet::new()
                    .run_in_state(GameView::Spacestation)
                    .with_system(set_next_active_player)
                    .with_system(redraw_on_new_active_changed)
                    .with_system(update_character_name)
                    .with_system(handle_change_state_button)
                    .with_system(handle_talk_button)
                    .into(),
            );
    }
}

#[derive(Component)]
struct ChangeCharacterButton {
    next: bool,
}

fn load_ship_ui(
    mut commands: Commands,
    font: Res<GameFont>,
    button_image: Res<ButtonImage>,
    cycle_button_images: Res<CycleButtonImages>,
    ui: Query<Entity, With<Ui>>,
    box_style: Query<&Style, With<UiBox>>,
) {
    let Ok(ui) = ui.get_single() else { return; };
    commands.entity(ui).despawn_descendants();

    let Ok(Val::Px(width)) = box_style
        .get_single()
        .map(|style| style.size.width)
        else { return; };

    let station_box = commands
        .spawn(NodeBundle {
            style: Style {
                size: Size {
                    height: Val::Px(width),
                    ..default()
                },
                display: Display::Flex,
                flex_direction: FlexDirection::Column,
                justify_content: JustifyContent::SpaceBetween,
                ..default()
            },
            ..default()
        })
        .id();

    let character_box = commands
        .spawn(NodeBundle {
            style: Style {
                flex_direction: FlexDirection::Row,
                align_items: AlignItems::Center,
                ..default()
            },
            ..default()
        })
        .id();

    let option_box = commands
        .spawn(NodeBundle {
            style: Style {
                flex_direction: FlexDirection::Column,
                size: Size {
                    height: Val::Px(VISIBLE_OPTIONS * 0.125 * width),
                    ..default()
                },
                ..default()
            },
            ..default()
        })
        .id();

    let character_children = vec![
        spawn_special_button_with_component(
            &mut commands,
            Style {
                size: Size {
                    height: Val::Px(23. * width / 128.),
                    width: Val::Px(13. * width / 128.),
                },
                ..default()
            },
            &cycle_button_images.prev,
            width,
            ChangeCharacterButton { next: false },
        ),
        spawn_special_button_with_component(
            &mut commands,
            Style {
                size: Size {
                    height: Val::Px(23. * width / 128.),
                    width: Val::Px(14. * width / 128.),
                },
                ..default()
            },
            &cycle_button_images.next,
            width,
            ChangeCharacterButton { next: true },
        ),
        commands
            .spawn((
                CharacterNameUi,
                TextBundle {
                    text: Text::from_section(
                        "",
                        TextStyle {
                            color: Color::AZURE,
                            font: font.0.clone(),
                            font_size: width * 0.126,
                        },
                    ),
                    style: Style {
                        margin: UiRect::left(Val::Percent(2.6)),
                        ..default()
                    },
                    ..default()
                },
            ))
            .id(),
    ];
    commands.entity(character_box).push_children(&character_children);

    let option_children = vec![
        spawn_button_with_component(
            &mut commands,
            "Talk".into(),
            &font.0,
            &button_image.0,
            width,
            TalkButton,
        ),
        spawn_button_with_component(
            &mut commands,
            "Missions".into(),
            &font.0,
            &button_image.0,
            width,
            ChangeStateButton(GameView::MissionSelect),
        ),
        spawn_button_with_component(
            &mut commands,
            "Cockpit".into(),
            &font.0,
            &button_image.0,
            width,
            ChangeStateButton(GameView::Cockpit),
        ),
    ];
    commands.entity(option_box).push_children(&option_children);

    let station_box_children = vec![character_box, option_box];
    commands.entity(station_box).push_children(&station_box_children);
    commands.entity(ui).add_child(station_box);
}

fn redraw_on_new_active_changed(
    mut commands: Commands,
    backgrounds: Res<BackgroundSprites>,
    mut scene: Query<(Entity, &mut Handle<Image>), With<Scene>>,
    new_active: Query<
        &Handle<Image>,
        (With<Character>, Added<Active>, Without<Scene>),
    >,
) {
    if new_active.is_empty() {
        return;
    }

    let (scene, mut image) = scene.get_single_mut().unwrap();
    spawn_lounge_scene(
        &mut commands,
        &backgrounds,
        new_active.iter().cloned().collect(),
        (scene, &mut image),
    );
}

fn set_next_active_player(
    mut commands: Commands,
    query: Query<(&Interaction, &ChangeCharacterButton), Changed<Interaction>>,
    alive: Query<
        (Entity, &CharacterName, Option<&Active>),
        (With<Character>, With<Alive>),
    >,
) {
    for (interaction, button) in query.iter() {
        if matches!(interaction, Interaction::Clicked) {
            let mut characters: Vec<(Entity, String, bool)> = alive
                .iter()
                .map(|(e, name, active)| (e, name.0.clone(), active.is_some()))
                .collect();

            characters.sort_by_key(|(_, name, _)| name.clone());

            if characters.is_empty() {
                return;
            }

            let active_idx = characters
                .iter()
                .position(|(_, _, active)| *active)
                .unwrap_or_default();

            let next_idx = if button.next {
                (active_idx + 1) % characters.len()
            } else {
                active_idx.checked_sub(1).unwrap_or(characters.len() - 1)
            };

            commands.entity(characters[active_idx].0).remove::<Active>();
            commands.entity(characters[next_idx].0).insert(Active);
        }
    }
}

fn init_station_state(
    mut commands: Commands,
    characters: Query<
        (Entity, Option<&Active>),
        (With<Character>, With<Alive>),
    >,
) {
    for (e, active) in characters.iter() {
        if active.is_some() {
            commands.entity(e).remove::<Active>();
            commands.entity(e).insert(Active);
            return;
        }
    }

    commands
        .entity(characters.iter().next().unwrap().0)
        .insert(Active);
}

fn update_character_name(
    mut text: Query<&mut Text, With<CharacterNameUi>>,
    active: Query<&CharacterName, With<Active>>,
) {
    if let Ok(name) = active.get_single() {
        if let Ok(mut text) = text.get_single_mut() {
            text.sections[0].value = name.0.clone();
        }
    }
}

#[derive(Component)]
struct TalkButton;

fn handle_talk_button(
    mut commands: Commands,
    query: Query<&Interaction, (With<TalkButton>, Changed<Interaction>)>,
    active: Query<&CharacterName, With<Active>>,
) {
    if !query
        .iter()
        .any(|interaction| matches!(interaction, Interaction::Clicked))
    {
        return;
    }

    let Ok(active) =  active.get_single() else {
        return;
    };

    match active.0.as_str() {
        "Patricia" => {
            commands.add(|w: &mut World| {
                let mut events_resource = w.resource_mut::<Events<_>>();
                events_resource.send(RequestTopicEvent::<Patricia>::default())
            });
        }
        "Stanley" => {
            commands.add(|w: &mut World| {
                let mut events_resource = w.resource_mut::<Events<_>>();
                events_resource.send(RequestTopicEvent::<Stanley>::default())
            });
        }
        "Nicholas" => {
            commands.add(|w: &mut World| {
                let mut events_resource = w.resource_mut::<Events<_>>();
                events_resource.send(RequestTopicEvent::<Nicholas>::default())
            });
        }
        "Nora" => {
            commands.add(|w: &mut World| {
                let mut events_resource = w.resource_mut::<Events<_>>();
                events_resource.send(RequestTopicEvent::<Nora>::default())
            });
        }
        _ => {}
    };
}
