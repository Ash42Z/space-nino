use crate::{GameFont, GameView, Ui, UiBox};

use bevy::prelude::*;
use iyes_loopless::prelude::*;

pub struct DeathPlugin;

impl Plugin for DeathPlugin {
    fn build(&self, app: &mut App) {
        app.add_enter_system(GameView::Death, on_death);
    }
}

fn on_death(
    mut commands: Commands,
    ui: Query<Entity, With<Ui>>,
    box_style: Query<&Style, With<UiBox>>,
    font: Res<GameFont>,
) {
    let text =
        "You return from the mission, only to discover the ship empty from life.";

    let Ok(ui) = ui.get_single() else { return; };
    commands.entity(ui).despawn_descendants();

    let Ok(Val::Px(width)) = box_style
        .get_single()
        .map(|style| style.size.width)
        else { return; };

    let children = vec![
        commands
            .spawn(TextBundle {
                text: Text::from_section(
                    text,
                    TextStyle {
                        color: Color::AZURE,
                        font: font.0.clone(),
                        font_size: width * 0.12,
                    },
                ),
                style: Style {
                    max_size: Size {
                        width: Val::Px(width * 0.96),
                        ..default()
                    },
                    margin: UiRect {
                        top: Val::Percent(2.),
                        left: Val::Percent(2.),
                        ..default()
                    },
                    ..default()
                },
                ..default()
            })
            .id(),
        commands
            .spawn(TextBundle {
                text: Text::from_section(
                    "THE END",
                    TextStyle {
                        color: Color::AZURE,
                        font: font.0.clone(),
                        font_size: width * 0.15,
                    },
                ),
                style: Style {
                    align_self: AlignSelf::Center,
                    max_size: Size {
                        width: Val::Px(width * 0.96),
                        ..default()
                    },
                    margin: UiRect {
                        top: Val::Percent(2.),
                        left: Val::Percent(2.),
                        ..default()
                    },
                    ..default()
                },
                ..default()
            })
            .id(),
    ];

    commands.entity(ui).push_children(&children);
}
