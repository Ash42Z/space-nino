use crate::{change_scene, BackgroundSprites, GameView};

use bevy::prelude::*;
use iyes_loopless::prelude::*;

pub fn spawn_button_with_component<C: Component>(
    commands: &mut Commands,
    text: &str,
    font: &Handle<Font>,
    image: &Handle<Image>,
    width: f32,
    comp: C,
) -> Entity {
    let button = spawn_button(commands, Some((text, font)), None, image, width);
    commands.entity(button).insert(comp);
    button
}

pub fn spawn_special_button_with_component<C: Component>(
    commands: &mut Commands,
    style: Style,
    image: &Handle<Image>,
    width: f32,
    comp: C,
) -> Entity {
    let button = spawn_button(commands, None, Some(style), image, width);
    commands.entity(button).insert(comp);
    button
}

pub fn spawn_button(
    commands: &mut Commands,
    text_params: Option<(&str, &Handle<Font>)>,
    style: Option<Style>,
    image: &Handle<Image>,
    width: f32,
) -> Entity {
    let style = style.unwrap_or_else(|| Style {
        size: Size {
            width: Val::Px(width),
            height: Val::Px(16. * width / 128.),
        },
        justify_content: JustifyContent::Center,
        align_items: AlignItems::Center,
        align_self: AlignSelf::Center,
        ..default()
    });

    let button = commands
        .spawn(ButtonBundle {
            image: UiImage(image.clone()),
            style,
            ..default()
        })
        .id();

    if let Some((text, font)) = text_params {
        let button_text = commands
            .spawn(TextBundle {
                text: Text::from_section(
                    text,
                    TextStyle {
                        color: Color::AZURE,
                        font: font.clone(),
                        font_size: width * 0.064,
                    },
                ),
                ..default()
            })
            .id();
        
        commands.entity(button).add_child(button_text);
    }
    
    button
}

#[derive(Component)]
pub struct ChangeStateButton(pub GameView);

pub fn handle_change_state_button(
    mut commands: Commands,
    query: Query<(&Interaction, &ChangeStateButton), Changed<Interaction>>,
) {
    for (interaction, state) in query.iter() {
        if matches!(interaction, Interaction::Clicked) {
            commands.insert_resource(NextState(state.0.clone()));
        }
    }
}

pub fn spawn_lounge_scene(
    commands: &mut Commands,
    backgrounds: &Res<BackgroundSprites>,
    characters: Vec<Handle<Image>>,
    scene: (Entity, &mut Handle<Image>),
) {
    change_scene(commands, scene.0, scene.1, &backgrounds.lounge);

    let translation_map = vec![
        vec![Vec3::new(-15., -30., 1.)],
        vec![Vec3::new(-15., -30., 1.), Vec3::new(15., -30., 1.)],
        vec![
            Vec3::new(-30., -35., 1.),
            Vec3::new(0., -30., 1.),
            Vec3::new(30., -40., 1.),
        ],
        vec![
            Vec3::new(-40., -40., 1.),
            Vec3::new(-15., -30., 1.),
            Vec3::new(15., -30., 1.),
            Vec3::new(40., -40., 1.),
        ],
    ];

    let len = characters.len();
    if len < 1 || len > translation_map.len() {
        return;
    }

    let children = characters
        .into_iter()
        .enumerate()
        .map(|(idx, char)| {
            commands
                .spawn(SpriteBundle {
                    texture: char,
                    transform: Transform {
                        translation: translation_map[len - 1][idx],
                        ..default()
                    },
                    ..default()
                })
                .id()
        })
        .collect::<Vec<_>>();

    commands.entity(scene.0).push_children(&children);
}
