#![feature(let_chains)]

use bevy::prelude::*;
use bevy::window::WindowResized;
use iyes_loopless::prelude::*;

mod utils;

mod title;
use title::TitleScreenPlugin;

mod intro;
use intro::IntroPlugin;

mod spacestation;
use spacestation::SpaceStationPlugin;

mod mission_select;
use mission_select::MissionSelectPlugin;

mod dialogue;

mod time;
use time::{TimePlugin, TimeUi};

mod cockpit;
use cockpit::CockpitPlugin;

mod gamestate;
use gamestate::GameStatePlugin;

mod topic;

mod patricia;
use patricia::PatriciaPlugin;

mod stanley;
use stanley::StanleyPlugin;

mod nicholas;
use nicholas::NicholasPlugin;

mod nora;
use nora::NoraPlugin;

mod mission;
use mission::{Mission, MissionPlugin};

mod encounter;
use encounter::{Encounter, EncounterPlugin};

mod descend;
use descend::DescendPlugin;

mod after;
use after::AfterPlugin;

mod shared_topics;
use shared_topics::SharedTopicsPlugin;

mod launch;
use launch::LaunchPlugin;

mod end;
use end::EndPlugin;

mod death;
use death::DeathPlugin;

#[derive(Clone, PartialEq, Eq, Debug, Hash)]
pub enum GameView {
    TitleScreen,
    Intro,
    Spacestation,
    Cockpit,
    MissionSelect,
    Descend(Mission, Option<Encounter>),
    AfterEncounter(Mission),
    AfterMission,
    Topic(Box<GameView>),
    Launch,
    End,
    Death,
}

#[derive(Resource)]
pub struct BackgroundSprites {
    title: Handle<Image>,
    cockpit: Handle<Image>,
    mission: Handle<Image>,
    lounge: Handle<Image>,
}

const SCENE_Z: i32 = 0;
const UI_Z: i32 = 2;

pub const SCENE_SIZE: u32 = 128;
pub const CHARACTER_SIZE: u32 = 32;

#[derive(Resource)]
pub struct GameFont(Handle<Font>);

#[derive(Component)]
pub struct UiBox;

#[derive(Component)]
pub struct Ui;

#[derive(Component)]
pub struct Scene;

pub fn change_scene(
    commands: &mut Commands,
    scene: Entity,
    image: &mut Handle<Image>,
    new_image: &Handle<Image>,
) {
    commands.entity(scene).despawn_descendants();
    *image = new_image.clone();
}

#[derive(Resource)]
pub struct ViewScale(f32);

#[derive(Resource)]
pub struct ButtonImage(Handle<Image>);

#[derive(Resource)]
pub struct CycleButtonImages {
    prev: Handle<Image>,
    next: Handle<Image>,
}

fn setup(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    audio: Res<Audio>,
) {
    commands.spawn(Camera2dBundle::default());

    commands.insert_resource(GameFont(asset_server.load("Abel-Regular.ttf")));

    commands.insert_resource(BackgroundSprites {
        title: asset_server.load("Title.png"),
        cockpit: asset_server.load("Cockpit.png"),
        mission: asset_server.load("Mission.png"),
        lounge: asset_server.load("Lounge.png"),
    });

    commands.insert_resource(ButtonImage(asset_server.load("Button.png")));

    commands.insert_resource(CycleButtonImages {
        prev: asset_server.load("PrevButton.png"),
        next: asset_server.load("NextButton.png"),
    });

    commands.insert_resource(ClearColor(Color::Rgba {
        red: 0.,
        blue: 0.,
        green: 0.,
        alpha: 1.,
    }));

    commands.insert_resource(ViewScale(1.0));
    commands.spawn((Scene, SpriteBundle::default()));

    commands
        .spawn((
            UiBox,
            NodeBundle {
                z_index: ZIndex::Global(UI_Z),
                style: Style {
                    flex_direction: FlexDirection::Column,
                    ..default()
                },
                ..default()
            },
        ))
        .with_children(|ui_box| {
            ui_box.spawn((TimeUi, NodeBundle::default()));
            ui_box.spawn((
                Ui,
                NodeBundle {
                    style: Style {
                        display: Display::Flex,
                        flex_direction: FlexDirection::Column,
                        ..default()
                    },
                    ..default()
                },
            ));
        });

    let music = asset_server.load("bgm.ogg");
    audio.play_with_settings(music, PlaybackSettings::LOOP);
}

fn resize_view(
    mut resize_ev: EventReader<WindowResized>,
    mut scale: ResMut<ViewScale>,
    mut scene: Query<&mut Transform, (With<Scene>, Without<UiImage>)>,
    mut ui_box: Query<&mut Style, With<UiBox>>,
    mut to_resize: Query<&mut Style, (With<Node>, Without<UiBox>, Without<Ui>)>,
    mut texts: Query<&mut Text>,
) {
    let Ok(mut scene) = scene.get_single_mut() else {
        return;
    };

    let Ok(mut ui_box) = ui_box.get_single_mut() else {
        return;
    };

    let prev_width = ui_box.size.width;

    for ev in resize_ev.iter() {
        let mut width = ev.width / 2.;
        if width > ev.height {
            width = ev.height;
        }

        scene.translation.x = -(width / 2.);
        scene.translation.y = 0.;
        scene.translation.z = SCENE_Z as f32;
        let scale_f = width / SCENE_SIZE as f32;
        scale.0 = scale_f as f32;

        scene.scale = Vec3::new(scale_f, scale_f, 1.);

        ui_box.position_type = PositionType::Absolute;
        ui_box.size = Size {
            width: Val::Px(width),
            height: Val::Px(width),
        };
        ui_box.position = UiRect {
            top: Val::Px((ev.height - width) / 2.),
            right: Val::Px(ev.width / 2. - width),
            ..default()
        };

        let Val::Px(prev_width_px) = prev_width else { continue; };
        let ratio = width / prev_width_px;

        for mut text in texts.iter_mut() {
            for mut section in text.sections.iter_mut() {
                section.style.font_size *= ratio;
            }
        }

        for mut element in to_resize.iter_mut() {
            element.size.height *= ratio;
            element.size.width *= ratio;
            element.max_size.height *= ratio;
            element.max_size.width *= ratio;
        }
    }
}

fn main() {
    let mut app = App::new();

    app.add_loopless_state(GameView::TitleScreen)
        .add_startup_system(setup)
        .add_system(resize_view)
        .add_plugins(DefaultPlugins.set(ImagePlugin::default_nearest()))
        .add_plugin(TitleScreenPlugin)
        .add_plugin(IntroPlugin)
        .add_plugin(SpaceStationPlugin)
        .add_plugin(MissionSelectPlugin)
        .add_plugin(TimePlugin)
        .add_plugin(CockpitPlugin)
        .add_plugin(GameStatePlugin)
        .add_plugin(PatriciaPlugin)
        .add_plugin(StanleyPlugin)
        .add_plugin(NicholasPlugin)
        .add_plugin(NoraPlugin)
        .add_plugin(MissionPlugin)
        .add_plugin(DescendPlugin)
        .add_plugin(EncounterPlugin)
        .add_plugin(AfterPlugin)
        .add_plugin(SharedTopicsPlugin)
        .add_plugin(LaunchPlugin)
        .add_plugin(DeathPlugin)
        .add_plugin(EndPlugin);

    #[cfg(target_arch = "wasm32")]
    {
        app.add_plugin(bevy_web_resizer::Plugin);
    }
    app.run();
}
