use crate::dialogue::DialogueData;
use crate::gamestate::ConversationIndex;
use crate::topic::{ChosenEvent, NextDialogue};
use crate::topic::{Topic, TopicPlugin};

use bevy::prelude::*;

use super::Stanley;

pub struct GenericPlugin;

impl Plugin for GenericPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(TopicPlugin::<Initial>::default())
            .add_system(handle_initial)
            .add_plugin(TopicPlugin::<RepeatResponse>::default());
    }
}

#[derive(Clone)]
pub enum Initial {
    Start,
}

impl Default for Initial {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for Initial {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![
                    ("What?", Some("Stanley")),
                    ("Take all the time you want.", Some("Stanley")),
                    ("Not like I care.", Some("Stanley")),
                    ("I never thought I had a chance to live anyway.", Some("Stanley")),
                    ("...", Some("Stanley")),
                    ("If you're planning on leaving again for a long time,", Some("Stanley")),
                    ("Maybe don't come back.", Some("Stanley")),
                ],
                vec![("Return", Some(NextDialogue::End))],
            ),
        }
    }
}

fn handle_initial(
    mut option_evs: EventReader<ChosenEvent<Initial>>,
    mut conversation: Query<&mut ConversationIndex, With<Stanley>>
) {
    for ev in option_evs.iter() {
        let mut conversation = conversation.get_single_mut().unwrap();
        if matches!(ev.current, Initial::Start) {
            conversation.0 += 1;
        }
    }
}

#[derive(Clone)]
pub enum RepeatResponse {
    Start,
}

impl Default for RepeatResponse {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for RepeatResponse {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![
                    ("I don't want to talk to you.", Some("Stanley")),
                    ("Could you stop looking at me?", Some("Stanley")),
                ],
                vec![("Return", Some(NextDialogue::End))],
            ),
        }
    }
}
