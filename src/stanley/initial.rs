use crate::dialogue::DialogueData;
use crate::gamestate::ConversationIndex;
use crate::topic::{ChosenEvent, NextDialogue};
use crate::topic::{Topic, TopicPlugin};

use bevy::prelude::*;

use super::Stanley;

pub struct GenericPlugin;

impl Plugin for GenericPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(TopicPlugin::<Initial>::default())
            .add_system(handle_initial)
            .add_plugin(TopicPlugin::<FirstResponse>::default())
            .add_system(handle_first_response)
            .add_plugin(TopicPlugin::<RepeatResponse>::default());
    }
}

#[derive(Clone)]
pub enum Initial {
    Start,
    Disaster,
    Together,
    Finish,
}

impl Default for Initial {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for Initial {
    fn data(&self) -> DialogueData<Self> {
        let middle_options = vec![
            (
                "We'll figure this out.",
                Some(NextDialogue::Next(Self::Finish)),
            ),
            (
                "Don't worry. I can fix this.",
                Some(NextDialogue::Next(Self::Finish)),
            ),
        ];

        match self {
            Self::Start => DialogueData::new(
                vec![
                    ("What makes you think I'll talk to you?", Some("Stanley")),
                    ("You just put me in a coffin.", Some("Stanley")),
                ],
                vec![
                    (
                        "Come on, it's not *that* bad",
                        Some(NextDialogue::Next(Self::Disaster)),
                    ),
                    (
                        "Hey, we're all in this together.",
                        Some(NextDialogue::Next(Self::Together)),
                    ),
                ],
            ),
            Self::Disaster => DialogueData::new(
                vec![(
                    "Bad? This is a disaster. I almost had a heart attack.",
                    Some("Stanley"),
                )],
                middle_options.clone(),
            ),
            Self::Together => DialogueData::new(
                vec![(
                    "Togther? I blame you. Of course I blame you. Who else?",
                    Some("Stanley"),
                )],
                middle_options.clone(),
            ),
            Self::Finish => DialogueData::new(
                vec![
                    ("Whatever. I'm a dead man.", Some("Stanley")),
                    (
                        "It's not like I have long to live anyway.",
                        Some("Stanley"),
                    ),
                ],
                vec![("Return", Some(NextDialogue::End))],
            ),
        }
    }
}

fn handle_initial(
    mut option_evs: EventReader<ChosenEvent<Initial>>,
    mut conversation: Query<&mut ConversationIndex, With<Stanley>>
) {
    for ev in option_evs.iter() {
        let mut conversation = conversation.get_single_mut().unwrap();
        if matches!(ev.current, Initial::Finish) {
            conversation.0 += 1;
        }
    }
}

#[derive(Clone)]
pub enum FirstResponse {
    Start,
}

impl Default for FirstResponse {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for FirstResponse {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![
                    ("What do you want from me?", Some("Stanley")),
                    ("Leave me alone.", Some("Stanley")),
                ],
                vec![("Return", Some(NextDialogue::End))],
            ),
        }
    }
}

fn handle_first_response(
    mut option_evs: EventReader<ChosenEvent<FirstResponse>>,
    mut conversation: Query<&mut ConversationIndex, With<Stanley>>
) {
    for ev in option_evs.iter() {
        let mut conversation = conversation.get_single_mut().unwrap();
        match ev.current {
            FirstResponse::Start => {
                conversation.0 += 1;
            }
        }
    }
}

#[derive(Clone)]
pub enum RepeatResponse {
    Start,
}

impl Default for RepeatResponse {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for RepeatResponse {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![("Haven't you done enough?", Some("Stanley"))],
                vec![("Return", Some(NextDialogue::End))],
            ),
        }
    }
}
