use crate::dialogue::DialogueData;
use crate::gamestate::ConversationIndex;
use crate::topic::{ChosenEvent, NextDialogue, Topic, TopicPlugin};

use bevy::prelude::*;

use super::Stanley;

pub struct GenericPlugin;

impl Plugin for GenericPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(TopicPlugin::<Initial>::default())
            .add_system(handle_initial)
            .add_plugin(TopicPlugin::<RepeatResponse>::default());
    }
}

#[derive(Clone)]
pub enum Initial {
    Start
}

impl Default for Initial {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for Initial {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![
                    ("Back in the good old days,", Some("Stanley")),
                    ("We had no intergalactic trips.", Some("Stanley")),
                    ("To keep busy, us kids had to put on our VR glasses and actually meet with each other.", Some("Stanley")),
                    ("We'd play soccer... We'd play catch...", Some("Stanley")),
                    ("Real interactions, in real places. Not all this convoluted sci-fi stuff.", Some("Stanley")),
                ],
                vec![
                    ("[Return]", Some(NextDialogue::End))
                ],
            )
        }
    }
}

fn handle_initial(
    mut option_evs: EventReader<ChosenEvent<Initial>>,
    mut conversation: Query<&mut ConversationIndex, With<Stanley>>
) {
    for ev in option_evs.iter() {
        let mut conversation = conversation.get_single_mut().unwrap();
        if matches!(ev.current, Initial::Start) {
            conversation.0 += 1;
        }
    }
}

#[derive(Clone)]
pub enum RepeatResponse {
    Start,
}

impl Default for RepeatResponse {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for RepeatResponse {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![
                    ("I used to be happy...", Some("Stanley")),
                ],
                vec![
                    ("[Return]", Some(NextDialogue::End))
                ],
            ),
        }
    }
}
