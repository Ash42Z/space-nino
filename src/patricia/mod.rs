use crate::gamestate::{
    Alive, CharacterBundle, CharacterName, ConversationIndex, Face,
    MissionCount, RequestTopicEvent, SidequestState, Trust,
};
use crate::topic::Topic;
use crate::GameView;

use bevy::prelude::*;

mod initial;
use initial as Initial;

mod second;
use second as Second;

mod generic2;
mod generic3;
mod generic4;
mod generic5;
mod generic6;
mod generic7;
mod generic8;

pub mod side_quest_0;
pub mod side_quest_1;
pub mod side_quest_2;

pub struct PatriciaPlugin;

impl Plugin for PatriciaPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(spawn)
            .add_system(dialogue)
            .add_event::<RequestTopicEvent<Patricia>>()
            .add_plugin(Initial::GenericPlugin)
            .add_plugin(Second::GenericPlugin)
            .add_plugin(generic2::GenericPlugin)
            .add_plugin(generic3::GenericPlugin)
            .add_plugin(generic4::GenericPlugin)
            .add_plugin(generic5::GenericPlugin)
            .add_plugin(generic6::GenericPlugin)
            .add_plugin(generic7::GenericPlugin)
            .add_plugin(generic8::GenericPlugin)
            .add_plugin(side_quest_0::Sidequest0Plugin)
            .add_plugin(side_quest_1::Sidequest1Plugin)
            .add_plugin(side_quest_2::Sidequest2Plugin);
    }
}

#[derive(Component)]
pub struct Patricia;

#[derive(Resource, Default)]
pub struct PatriciaState {
    pub zeroth_sidequest: SidequestState,
    pub first_sidequest: SidequestState,
    pub second_sidequest: SidequestState,
}

fn spawn(mut commands: Commands, asset_server: Res<AssetServer>) {
    commands.spawn((
        CharacterBundle {
            name: CharacterName::new("Patricia"),
            trust: Trust(0.5),
            image: asset_server.load("Patricia1.png"),
            face: Face(asset_server.load("PatriciaFace1.png")),
            ..default()
        },
        Patricia,
        Alive,
        ConversationIndex(0),
    ));

    commands.insert_resource(PatriciaState::default());
}

fn dialogue(
    mut commands: Commands,
    mut evs: EventReader<RequestTopicEvent<Patricia>>,
    state: Res<PatriciaState>,
    conversation: Query<&ConversationIndex, With<Patricia>>,
    mission_count: Res<MissionCount>,
) {
    if evs.iter().next().is_none() {
        return;
    }

    if state.zeroth_sidequest == SidequestState::Finished
        && state.first_sidequest == SidequestState::NotGiven
    {
        side_quest_1::GiveQuest::spawn(&mut commands, GameView::Spacestation);
        return;
    }

    if state.first_sidequest == SidequestState::Finished
        && state.second_sidequest == SidequestState::NotGiven
    {
        side_quest_2::GiveQuest::spawn(&mut commands, GameView::Spacestation);
        return;
    }


    let conversation = conversation.get_single().unwrap();

    match (mission_count.0, conversation.0) {
        (0, 0) => {
            Initial::Initial::spawn(&mut commands, GameView::Spacestation)
        }
        (0, 1) => Initial::RepeatResponse::spawn(
            &mut commands,
            GameView::Spacestation,
        ),
        (1, 0) => Second::Initial::spawn(&mut commands, GameView::Spacestation),
        (1, 1) => {
            Second::RepeatResponse::spawn(&mut commands, GameView::Spacestation)
        }
        (2, 0) => {
            generic2::Initial::spawn(&mut commands, GameView::Spacestation)
        }
        (2, 1) => generic2::RepeatResponse::spawn(
            &mut commands,
            GameView::Spacestation,
        ),
        (3, 0) => {
            generic3::Initial::spawn(&mut commands, GameView::Spacestation)
        }
        (3, 1) => generic3::RepeatResponse::spawn(
            &mut commands,
            GameView::Spacestation,
        ),
        (4, 0) => {
            generic4::Initial::spawn(&mut commands, GameView::Spacestation)
        }
        (4, 1) => generic4::RepeatResponse::spawn(
            &mut commands,
            GameView::Spacestation,
        ),
        (5, 0) => {
            generic5::Initial::spawn(&mut commands, GameView::Spacestation)
        }
        (5, 1) => generic5::RepeatResponse::spawn(
            &mut commands,
            GameView::Spacestation,
        ),
        (6, 0) => {
            generic6::Initial::spawn(&mut commands, GameView::Spacestation)
        }
        (6, 1) => generic6::RepeatResponse::spawn(
            &mut commands,
            GameView::Spacestation,
        ),
        (7, 0) => {
            generic7::Initial::spawn(&mut commands, GameView::Spacestation)
        }
        (7, 1) => generic7::RepeatResponse::spawn(
            &mut commands,
            GameView::Spacestation,
        ),
        (8, 0) => {
            generic8::Initial::spawn(&mut commands, GameView::Spacestation)
        }
        (8, 1) => generic8::RepeatResponse::spawn(
            &mut commands,
            GameView::Spacestation,
        ),
        _ => {}
    }
}
