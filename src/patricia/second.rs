use crate::dialogue::DialogueData;
use crate::gamestate::{SidequestState, ConversationIndex};
use crate::topic::{ChosenEvent, NextDialogue, Topic, TopicPlugin};
use crate::patricia::PatriciaState;

use bevy::prelude::*;

use super::Patricia;

pub struct GenericPlugin;

impl Plugin for GenericPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(TopicPlugin::<Initial>::default())
            .add_system(handle_initial)
            .add_plugin(TopicPlugin::<RepeatResponse>::default())
            .add_system(handle_repeat);
    }
}

#[derive(Clone)]
pub enum Initial {
    Start,
    Finish,
}

impl Default for Initial {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for Initial {
    fn data(&self) -> DialogueData<Self> {
        let goto_finish = Some(NextDialogue::Next(Self::Finish));

        match self {
            Self::Start => DialogueData::new(
                vec![
                    ("I can't believe I didn't think of it before.", Some("Patricia")),
                    ("This is fantastic!", Some("Patricia")),
                    ("Experiencing differences in time dilation.", Some("Patricia")),
                    ("Who knew we'd live to see the day?", Some("Patricia")),
                ],
                vec![
                    ("I know, right?", goto_finish.clone()),
                    ("I still can't belive it", goto_finish.clone())
                ],
            ),
            Self::Finish => DialogueData::new(
                vec![
                    ("When you gather data, do it carefully.", Some("Patricia")),
                    ("Take your time.", Some("Patricia")),
                    ("The only way we survive is if we gather accurate data,", Some("Patricia")),
                    ("and as much of it as possible.", Some("Patricia")),
                    ("So if you want some tips about interesting data collection locations, talk to me.", Some("Patricia")),
                ],
                vec![("Return", Some(NextDialogue::End))],
            ),
        }
    }
}

fn handle_initial(
    mut option_evs: EventReader<ChosenEvent<Initial>>,
    mut conversation: Query<&mut ConversationIndex, With<Patricia>>
) {
    let mut conversation = conversation.get_single_mut().unwrap();

    for ev in option_evs.iter() {
        if matches!(ev.current, Initial::Finish) {
            conversation.0 += 1;
        }
    }
}

#[derive(Clone)]
pub enum RepeatResponse {
    Start,
}

impl Default for RepeatResponse {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for RepeatResponse {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![
                    ("Oh, did you come back for some tips?", Some("Patricia")),
                    ("You should gather navigation data in the Lorentz orbit sphere", Some("Patricia")),
                    ("It's clearly the best research opportunity right now.", Some("Patricia")),
                    ("I've sent the details to your computer", Some("Patricia"))
                ],
                vec![("Return", Some(NextDialogue::End))],
            ),
        }
    }
}

fn handle_repeat(
    mut option_evs: EventReader<ChosenEvent<RepeatResponse>>,
    mut state: ResMut<PatriciaState>,
) {
    for ev in option_evs.iter() {
        if matches!(ev.current, RepeatResponse::Start) {
            state.zeroth_sidequest = SidequestState::Given;
        }
    }
}
