use crate::dialogue::DialogueData;
use crate::gamestate::{NavData, SidequestState, Trust};
use crate::mission::{CurrentMission, NavDataAmount};
use crate::patricia::{Patricia, PatriciaState};
use crate::time::{add_seconds, StationTime};
use crate::topic::{ChosenEvent, NextDialogue, Topic, TopicPlugin};

use bevy::prelude::*;
use iyes_loopless::prelude::*;

pub struct Sidequest0Plugin;

impl Plugin for Sidequest0Plugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(TopicPlugin::<Sidequest0>::default())
            .add_system(handle.run_if_resource_exists::<CurrentMission>());
    }
}

#[derive(Clone)]
pub enum Sidequest0 {
    Arrive,
    Collected,
}

impl Default for Sidequest0 {
    fn default() -> Self {
        Self::Arrive
    }
}

impl Topic for Sidequest0 {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Arrive => DialogueData::new(
                vec![
                    ("You arrived to the orbit Patricia requested.", None),
                    ("Sensors are not reading a whole lot..", None),
                ],
                vec![(
                    "Collect navigational data.",
                    Some(NextDialogue::Next(Self::Collected)),
                )],
            ),
            Self::Collected => DialogueData::new(
                vec![(
                    "You've collected the data. This was not very helpful.",
                    None,
                )],
                vec![("[Return]", Some(NextDialogue::End))],
            ),
        }
    }
}

pub fn handle(
    mut chosen_evs: EventReader<ChosenEvent<Sidequest0>>,
    mut time: ResMut<StationTime>,
    mut nav: ResMut<NavData>,
    mut state: ResMut<PatriciaState>,
    mission: Res<CurrentMission>,
    mut trust: Query<&mut Trust, With<Patricia>>
) {
    for ev in chosen_evs.iter() {
        match ev.current {
            Sidequest0::Arrive => {
                add_seconds(&mut time, mission.0.mission_time() as f64);
            }
            Sidequest0::Collected => {
                nav.0 += NavDataAmount::Low.value();
                state.zeroth_sidequest = SidequestState::Finished;
                if let Ok(mut trust) = trust.get_single_mut() {
                    trust.0 += 0.2;
                }
            }
        }
    }
}
