use crate::dialogue::DialogueData;
use crate::gamestate::ConversationIndex;
use crate::topic::{ChosenEvent, NextDialogue, Topic, TopicPlugin};

use bevy::prelude::*;

use super::Patricia;

pub struct GenericPlugin;

impl Plugin for GenericPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(TopicPlugin::<Initial>::default())
            .add_system(handle_initial)
            .add_plugin(TopicPlugin::<RepeatResponse>::default());
    }
}

#[derive(Clone)]
pub enum Initial {
    Start
}

impl Default for Initial {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for Initial {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![
                    ("Me?", Some("Patricia")),
                    ("I'm fine.", Some("Patricia")),
                    ("More than fine.", Some("Patricia")),
                    ("This ship is a really good opportunity to work on my research.", Some("Patricia")),
                    ("No one has stayed so long next to a black hole.", Some("Patricia")),
                    ("How could I be disappointed?", Some("Patricia")),
                    ("I really hope my discoveries will get back to Earth.", Some("Patricia"))
                ],
                vec![("[Return]", Some(NextDialogue::End))],
            )
        }
    }
}

fn handle_initial(
    mut option_evs: EventReader<ChosenEvent<Initial>>,
    mut conversation: Query<&mut ConversationIndex, With<Patricia>>
) {
    let mut conversation = conversation.get_single_mut().unwrap();

    for ev in option_evs.iter() {
        if matches!(ev.current, Initial::Start) {
            conversation.0 += 1;
        }
    }
}

#[derive(Clone)]
pub enum RepeatResponse {
    Start,
}

impl Default for RepeatResponse {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for RepeatResponse {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![
                    ("Take your time.", Some("Patricia")),
                    ("I have enough to keep myself busy.", Some("Patricia"))
                ],
                vec![("[Return]", Some(NextDialogue::End))],
            ),
        }
    }
}
