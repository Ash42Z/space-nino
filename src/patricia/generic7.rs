use crate::dialogue::DialogueData;
use crate::gamestate::ConversationIndex;
use crate::topic::{ChosenEvent, NextDialogue, Topic, TopicPlugin};

use bevy::prelude::*;

use super::Patricia;

pub struct GenericPlugin;

impl Plugin for GenericPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(TopicPlugin::<Initial>::default())
            .add_system(handle_initial)
            .add_plugin(TopicPlugin::<RepeatResponse>::default());
    }
}

#[derive(Clone)]
pub enum Initial {
    Start
}

impl Default for Initial {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for Initial {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![
                    ("It's too bad our ship has an artificial gravitational field.", Some("Patricia")),
                    ("There are so many interesting experiments I could run in zero gravity.", Some("Patricia")),
                    ("Could we turn off the field? Just for a couple minutes.", Some("Patricia")),
                    ("Wait... No...", Some("Patricia")),
                    ("That would make us fall in the black hole, wouldn't it?", Some("Patricia")),
                    ("...", Some("Patricia")),
                    ("Maybe that wasn't such a bright idea.", Some("Patricia")),
                ],
                vec![("[Return]", Some(NextDialogue::End))],
            )
        }
    }
}

fn handle_initial(
    mut option_evs: EventReader<ChosenEvent<Initial>>,
    mut conversation: Query<&mut ConversationIndex, With<Patricia>>
) {
    let mut conversation = conversation.get_single_mut().unwrap();

    for ev in option_evs.iter() {
        if matches!(ev.current, Initial::Start) {
            conversation.0 += 1;
        }
    }
}

#[derive(Clone)]
pub enum RepeatResponse {
    Start
}

impl Default for RepeatResponse {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for RepeatResponse {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![
                    ("I'll get back to you if I think of something better.", Some("Patricia"))
                ],
                vec![
                    ("[Return]", Some(NextDialogue::End))
                ]
            ),
        }
    }
}
