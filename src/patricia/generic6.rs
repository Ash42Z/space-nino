use crate::dialogue::DialogueData;
use crate::gamestate::ConversationIndex;
use crate::topic::{ChosenEvent, NextDialogue, Topic, TopicPlugin};

use bevy::prelude::*;

use super::Patricia;

pub struct GenericPlugin;

impl Plugin for GenericPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(TopicPlugin::<Initial>::default())
            .add_system(handle_initial)
            .add_plugin(TopicPlugin::<RepeatResponse>::default());
    }
}

#[derive(Clone)]
pub enum Initial {
    Start
}

impl Default for Initial {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for Initial {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![
                    ("Space is so interesting.", Some("Patricia")),
                    ("I've been discovering SO MANY new things!", Some("Patricia")),
                    ("I sometimes wonder if we should even keep trying to get back home.", Some("Patricia")),
                    ("...", Some("Patricia")),
                    ("......", Some("Patricia")),
                    (".........", Some("Patricia")),
                    ("Don't worry. It's just a thought.", Some("Patricia")),
                ],
                vec![("[Return]", Some(NextDialogue::End))],
            )
        }
    }
}

fn handle_initial(
    mut option_evs: EventReader<ChosenEvent<Initial>>,
    mut conversation: Query<&mut ConversationIndex, With<Patricia>>
) {
    let mut conversation = conversation.get_single_mut().unwrap();

    for ev in option_evs.iter() {
        if matches!(ev.current, Initial::Start) {
            conversation.0 += 1;
        }
    }
}

#[derive(Clone)]
pub enum RepeatResponse {
    Start
}

impl Default for RepeatResponse {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for RepeatResponse {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![
                    ("I hope you didn't take my thought about staying here too seriously.", Some("Patricia")),
                    ("...", Some("Patricia")),
                    ("......", Some("Patricia")),
                    ("But if you did, let me know.", Some("Patricia")),
                ],
                vec![
                    ("[Return]", Some(NextDialogue::End))
                ]
            ),
        }
    }
}
