use crate::dialogue::DialogueData;
use crate::gamestate::ConversationIndex;
use crate::topic::{ChosenEvent, NextDialogue, Topic, TopicPlugin};

use bevy::prelude::*;

use super::Patricia;

pub struct GenericPlugin;

impl Plugin for GenericPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(TopicPlugin::<Initial>::default())
            .add_system(handle_initial)
            .add_plugin(TopicPlugin::<RepeatResponse>::default());
    }
}

#[derive(Clone)]
pub enum Initial {
    Start
}

impl Default for Initial {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for Initial {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![
                    ("Did you know?", Some("Patricia")),
                    ("At Earth I used to be an astrophysicist.", Some("Patricia")),
                    ("I researched small partical behavior near black hole event horizons.", Some("Patricia")),
                    ("This is in fact what made me go on this trip.", Some("Patricia")),
                    ("It's fascinating. Seeing a black hole.", Some("Patricia")),
                    ("Especially once you understand all the physical phenomena.", Some("Patricia")),
                    ("I can tell you more, if you'd like.", Some("Patricia")),
                ],
                vec![("[Return]", Some(NextDialogue::End))],
            )
        }
    }
}

fn handle_initial(
    mut option_evs: EventReader<ChosenEvent<Initial>>,
    mut conversation: Query<&mut ConversationIndex, With<Patricia>>
) {
    let mut conversation = conversation.get_single_mut().unwrap();

    for ev in option_evs.iter() {
        if matches!(ev.current, Initial::Start) {
            conversation.0 += 1;
        }
    }
}

#[derive(Clone)]
pub enum RepeatResponse {
    Start,
    Later,
    Sure
}

impl Default for RepeatResponse {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for RepeatResponse {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![
                    ("Hey! Did you come back to hear more about black holes?", Some("Patricia")),
                ],
                vec![("Maybe later", Some(NextDialogue::Next(Self::Later))),
                     ("Sure", Some(NextDialogue::Next(Self::Sure)))],
            ),
            Self::Later => DialogueData::new(
                vec![("Oh, okay.", Some("Patricia")),
                ("Let me know when you do.", Some("Patricia"))],
                vec![("[Return]", Some(NextDialogue::End))]
            ),
            Self::Sure => DialogueData::new(
                vec![
                    ("Awesome!", Some("Patricia")),
                    ("Okay okay.", Some("Patricia")),
                    ("See that light ring around the black hole?", Some("Patricia")),
                    ("It's made out of [some unclear scientific term].", Some("Patricia")),
                    ("And the light is created from [some other sci-fi jibber jabber].", Some("Patricia")),
                    ("Patricia drifts off-topic and rambles on about random sciency stuff you do not have the capability to understand.", None),
                    ("At some point you simply walk away.", None),
                    ("(Patricia continues to talk to herself)", None)
                ],
                vec![
                    ("[Return]", Some(NextDialogue::End))
                ]
            )
        }
    }
}
