use crate::dialogue::DialogueData;
use crate::topic::{ChosenEvent, NextDialogue, Topic, TopicPlugin};
use crate::patricia::{Patricia, PatriciaState};
use crate::gamestate::{SidequestState, Fuel, Trust};
use crate::time::{StationTime, add_seconds};
use crate::mission::CurrentMission;

use bevy::prelude::*;
use iyes_loopless::prelude::*;

pub struct Sidequest2Plugin;

impl Plugin for Sidequest2Plugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(TopicPlugin::<GiveQuest>::default())
            .add_system(handle)
            .add_plugin(TopicPlugin::<Sidequest2>::default())
            .add_system(handle_mission.run_if_resource_exists::<CurrentMission>());
    }
}

#[derive(Clone)]
pub enum GiveQuest {
    Start,
    GoForIt,
    Another,
    Next,
    Important,
    WastingTime,
    What,
    Finish
}

impl Default for GiveQuest {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for GiveQuest {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![
                    ("Oh hey, I didn't even noticed you came back!", Some("Patricia")),
                    ("I'm deep into my research lately.", Some("Patricia")),
                    ("Its not like there's anything else to do here, you know...", Some("Patricia")),
                    ("Anyhow", Some("Patricia")),
                    ("I had another really good idea for a research mission.", Some("Patricia")),
                    ("Are you ready?", Some("Patricia")), 
                ],
                vec![
                    ("Of course!", Some(NextDialogue::Next(Self::GoForIt))),
                    ("Another one?", Some(NextDialogue::Next(Self::Another)))
                ],
            ),
            Self::GoForIt => DialogueData::new(
                vec![("Great!", Some("Patricia"))],
                vec![("Next", Some(NextDialogue::Next(Self::Next)))],
            ),
            Self::Another => DialogueData::new(
                vec![
                    ("Don't be like this...", Some("Patricia")),
                    ("Just listen", Some("Patricia"))],
                vec![("Next", Some(NextDialogue::Next(Self::Next)))],
            ),
            Self::Next => DialogueData::new(
                vec![
                    ("So", Some("Patricia")),
                    ("I was working hard during your last mission about the possibility of life around a black hole.", Some("Patricia")),
                    ("And I found out that really close to us there used to be a planet.", Some("Patricia")),
                    ("Well", Some("Patricia")),
                    ("More like a white dwarf", Some("Patricia")),
                    ("Which contained a lot of water", Some("Patricia")),
                    ("In a gas form of obviously", Some("Patricia")),
                    ("And when it collapsed into the black hole next to us it also spread some its water vapor in an orbit around the black hole", Some("Patricia")),
                    ("We have a really good opportunity to bring some of it back for some research!", Some("Patricia")),
                    ("What do you say?", Some("Patricia")),
                ],
                vec![
                    ("Sound really important, I'm in.", Some(NextDialogue::Next(Self::Important))),
                    ("Don't you think this is a little waste of time?", Some(NextDialogue::Next(Self::WastingTime))),
                    ("Supernova what?", Some(NextDialogue::Next(Self::What)))
                ],
            ),
    
            Self::Important => DialogueData::new(
                vec![("Wonderfull!", Some("Patricia"))],
                vec![("Next", Some(NextDialogue::Next(Self::Finish)))],
            ),
            Self::WastingTime => DialogueData::new(
                vec![
                    ("We have enough time to fix the ship later.", Some("Patricia")),
                    ("This big discovery happens one in a millennium!", Some("Patricia"))
                ],
                vec![("Next", Some(NextDialogue::Next(Self::Finish)))],
            ),
            Self::What => DialogueData::new(
                vec![("You'll understand when you'll see it.", Some("Patricia"))],
                vec![("Next", Some(NextDialogue::Next(Self::Finish)))],
            ),
            Self::Finish => DialogueData::new(
                vec![
                    ("I've already entered the data to your computer so you don't miss it.", Some("Patricia")),
                    ("Good luck!", Some("Patricia"))
                ],
                vec![("[Return]", Some(NextDialogue::End))],
            ),
        }
    }
}

fn handle(
    mut option_evs: EventReader<ChosenEvent<GiveQuest>>,
    mut state: ResMut<PatriciaState>,
) {
    for ev in option_evs.iter() {
        if matches!(ev.current, GiveQuest::Finish) {
            state.second_sidequest = SidequestState::Given;
        }
    }
}

#[derive(Clone)]
pub enum Sidequest2 {
    Arrive,
    Collected,
}


impl Default for Sidequest2 {
    fn default() -> Self {
        Self::Arrive
    }
}


impl Topic for Sidequest2 {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Arrive => DialogueData::new(
                vec![
                    ("You found shards of ice oribiting the black hole.", None),
                ],
                vec![(
                    "Collect ice for patricia.",
                    Some(NextDialogue::Next(Self::Collected)),
                )],
            ),
            Self::Collected => DialogueData::new(
                vec![(
                    "Upon collecting the sample, the ship's radar detected a large amount of plutonium embedded within the ice. \
                    Did Patricia know about this?",
                    None,
                )],
                vec![("Incredible", Some(NextDialogue::End))],
            ),
        }
    }
}

pub fn handle_mission(
    mut chosen_evs: EventReader<ChosenEvent<Sidequest2>>,
    mut time: ResMut<StationTime>,
    mut state: ResMut<PatriciaState>,
    mut fuel: ResMut<Fuel>,
    mission: Res<CurrentMission>,
    mut trust: Query<&mut Trust, With<Patricia>>
) {
    for ev in chosen_evs.iter() {
        match ev.current {
            Sidequest2::Arrive => {
                add_seconds(&mut time, mission.0.mission_time() as f64);
            }
            Sidequest2::Collected => {
                fuel.0 += 5;
                state.second_sidequest = SidequestState::Finished;
                if let Ok(mut trust) = trust.get_single_mut() {
                    trust.0 += 0.2;
                }
            }
        }
    }
}
