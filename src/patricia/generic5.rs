use crate::dialogue::DialogueData;
use crate::gamestate::ConversationIndex;
use crate::topic::{ChosenEvent, NextDialogue, Topic, TopicPlugin};

use bevy::prelude::*;

use super::Patricia;

pub struct GenericPlugin;

impl Plugin for GenericPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(TopicPlugin::<Initial>::default())
            .add_system(handle_initial)
            .add_plugin(TopicPlugin::<RepeatResponse>::default());
    }
}

#[derive(Clone)]
pub enum Initial {
    Start
}

impl Default for Initial {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for Initial {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![
                    ("I really like it here, you know.", Some("Patricia")),
                    ("Something about the endless silence...", Some("Patricia")),
                    ("Makes you think about life.", Some("Patricia")),
                    ("Are humans just extensions of Earth's nervous system?", Some("Patricia")),
                    ("If the laws of the universe were ever so slightly different...?", Some("Patricia")),
                    ("...would life still exist as we know it?", Some("Patricia")),
                    ("Is this life even real?", Some("Patricia")),
                    ("Or are we in a simulation?", Some("Patricia")),
                    ("And if so... Can we run the simulation?", Some("Patricia")),
                    ("And will the people in that simulation run the simulation themselves?", Some("Patricia")),
                    ("...", Some("Patricia")),
                    ("I wonder if the others think about this stuff as well.", Some("Patricia")),
                ],
                vec![("[Return]", Some(NextDialogue::End))],
            )
        }
    }
}

fn handle_initial(
    mut option_evs: EventReader<ChosenEvent<Initial>>,
    mut conversation: Query<&mut ConversationIndex, With<Patricia>>
) {
    let mut conversation = conversation.get_single_mut().unwrap();

    for ev in option_evs.iter() {
        if matches!(ev.current, Initial::Start) {
            conversation.0 += 1;
        }
    }
}

#[derive(Clone)]
pub enum RepeatResponse {
    Start
}

impl Default for RepeatResponse {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for RepeatResponse {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![
                    ("I wonder what you would think about, if you were also stuck with us for years on end...", Some("Patricia")),
                ],
                vec![
                    ("[Return]", Some(NextDialogue::End))
                ]
            ),
        }
    }
}
