use crate::dialogue::DialogueData;
use crate::gamestate::ConversationIndex;
use crate::topic::{ChosenEvent, NextDialogue, Topic, TopicPlugin};

use bevy::prelude::*;

use super::Patricia;

pub struct GenericPlugin;

impl Plugin for GenericPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(TopicPlugin::<Initial>::default())
            .add_system(handle_initial)
            .add_plugin(TopicPlugin::<RepeatResponse>::default());
    }
}

#[derive(Clone)]
pub enum Initial {
    Start,
    RealTalk,
    Finish,
}

impl Default for Initial {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for Initial {
    fn data(&self) -> DialogueData<Self> {
        let goto_realtalk = Some(NextDialogue::Next(Self::RealTalk));

        match self {
            Self::Start => DialogueData::new(
                vec![
                    ("How are you going to sort this out?", Some("Patricia"))
                ],
                vec![
                    ("[Use scientific jargon to buy time]", goto_realtalk.clone()),
                    ("I don't know", Some(NextDialogue::Next(Self::Finish)))
                ],
            ),
            Self::RealTalk => DialogueData::new(
                vec![
                    ("Don't try those tricks around me.", Some("Patricia")),
                    (
                        "I understand exactly what you're saying. I know it's utter nonsense.",
                        Some("Patricia")
                    )
                ],
                vec![(
                    "Honestly, I'm not sure yet.",
                    Some(NextDialogue::Next(Self::Finish))
                )],
            ),
            Self::Finish => DialogueData::new(
                vec![
                    (
                        "Well, you better think of something, fast.",
                        Some("Patricia")
                    ),
                    (
                        "I don't know how long we can survive above a black hole.",
                        Some("Patricia")
                    ),
                    ("But it can't be long.", Some("Patricia")),
                ],
                vec![("Return", Some(NextDialogue::End))],
            ),
        }
    }
}

fn handle_initial(
    mut option_evs: EventReader<ChosenEvent<Initial>>,
    mut conversation: Query<&mut ConversationIndex, With<Patricia>>
) {
    let mut conversation = conversation.get_single_mut().unwrap();

    for ev in option_evs.iter() {
        if matches!(ev.current, Initial::Finish) {
            conversation.0 += 1;
        }
    }
}

#[derive(Clone)]
pub enum RepeatResponse {
    Start,
}

impl Default for RepeatResponse {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for RepeatResponse {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![("Did you figure it out yet?", Some("Patricia"))],
                vec![("Return", Some(NextDialogue::End))],
            ),
        }
    }
}
