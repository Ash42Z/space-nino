use crate::dialogue::DialogueData;
use crate::gamestate::{SidequestState, Trust};
use crate::mission::CurrentMission;
use crate::time::{add_seconds, StationTime};
use crate::topic::{ChosenEvent, NextDialogue, Topic, TopicPlugin};

use crate::patricia::{PatriciaState, Patricia};

use bevy::prelude::*;
use iyes_loopless::prelude::*;

pub struct Sidequest1Plugin;

impl Plugin for Sidequest1Plugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(TopicPlugin::<GiveQuest>::default())
            .add_system(handle)
            .add_plugin(TopicPlugin::<Sidequest1>::default())
            .add_system(
                handle_mission.run_if_resource_exists::<CurrentMission>(),
            );
    }
}

#[derive(Clone)]
pub enum GiveQuest {
    Start,
    OfCourse,
    Fine,
    Awful,
    Next,
    Idea,
    Amazing,
    Important,
    What,
    Finish,
}

impl Default for GiveQuest {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for GiveQuest {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![
                    ("Hey!", Some("Patricia")),
                    ("Isn't space wonderful?", Some("Patricia")),
                    ("There is so many thing to research around here.", Some("Patricia")),
                    ("And now I have the time as well.", Some("Patricia")),
                    ("By the way, how was your last journey? Was my recommendation useful?", Some("Patricia")),
                ],
                vec![
                    ("Of course! I got so much data!", Some(NextDialogue::Next(Self::OfCourse))),
                    ("Not something special.", Some(NextDialogue::Next(Self::Fine))),
                    ("It was awful.", Some(NextDialogue::Next(Self::Awful)))
                ],
            ),
            Self::OfCourse => DialogueData::new(
                vec![
                    ("I'm not suprised.", Some("Patricia")),
                    ("The interference between the cosmic radiation and the gravitational waves can really help with callibrating navigation systems.", Some("Patricia")),
                ],
                vec![("Clearly", Some(NextDialogue::Next(Self::Next)))],
            ),
            Self::Fine => DialogueData::new(
                vec![
                    ("Interesting.", Some("Patricia")),
                    ("Maybe the solar burst changed the interference pattern and harm the callibration.", Some("Patricia")),
                ],
                vec![("Maybe", Some(NextDialogue::Next(Self::Next)))],
            ),
            Self::Awful => DialogueData::new(
                vec![
                    ("Weird.", Some("Patricia")),
                    ("Maybe I had an error in my calculation.", Some("Patricia")),
                    ("I'll check it later.", Some("Patricia")),
                ],
                vec![("Ok", Some(NextDialogue::Next(Self::Next)))],
            ),
            Self::Next => DialogueData::new(
                vec![
                    ("Anyways.", Some("Patricia")),
                    ("I had another idea for a good mission.", Some("Patricia")),
                    ("A REALLY good mission.", Some("Patricia")),
                    ("Are you ready?", Some("Patricia")),
                ],
                vec![("Whats your idea?", Some(NextDialogue::Next(Self::Idea))),
                ("Talk to me!", Some(NextDialogue::Next(Self::Idea)))],
            ),
            Self::Idea => DialogueData::new(
                vec![
                    ("So", Some("Patricia")),
                    ("In 2098, Laybre - the Nobel winner astrophysicist came up with a theory", Some("Patricia")),
                    ("It says thatmolecules near a black hole can be made entirely of
                    anti-dark-matter.", Some("Patricia")),
                    ("It's really difficult to measure that so in years no one was able to test this theory.", Some("Patricia")),
                    ("Well", Some("Patricia")),
                    ("Until now!", Some("Patricia")),
                    ("I've been able to find an astroid which contains that!", Some("Patricia")),
                    ("Isn't that exciting?", Some("Patricia")),
                ],
                vec![
                    ("That sounds amazing!", Some(NextDialogue::Next(Self::Amazing))),
                    ("Why is it important?", Some(NextDialogue::Next(Self::Important))),
                    ("Anti-dark-what?", Some(NextDialogue::Next(Self::What)))],
            ),
            Self::Amazing => DialogueData::new(
                vec![("I know, right?", Some("Patricia"))],
                vec![("Next", Some(NextDialogue::Next(Self::Finish)))],
            ),
            Self::Important => DialogueData::new(
                vec![("For science, of course", Some("Patricia"))],
                vec![("Next", Some(NextDialogue::Next(Self::Finish)))],
            ),
            Self::What => DialogueData::new(
                vec![("Never mind", Some("Patricia")),
                ("It just a really important discovery.", Some("Patricia"))],
                vec![("Next", Some(NextDialogue::Next(Self::Finish)))],
            ),
            Self::Finish => DialogueData::new(
                vec![("I've added the coordinates to your computer", Some("Patricia")),
                ("Make sure not to miss this oportunity!", Some("Patricia"))],
                vec![("[Return]", Some(NextDialogue::End))],
            ),
        }
    }
}

fn handle(
    mut option_evs: EventReader<ChosenEvent<GiveQuest>>,
    mut state: ResMut<PatriciaState>,
) {
    for ev in option_evs.iter() {
        if matches!(ev.current, GiveQuest::Finish) {
            state.first_sidequest = SidequestState::Given;
        }
    }
}

#[derive(Clone)]
pub enum Sidequest1 {
    Arrive,
    Collected,
}

impl Default for Sidequest1 {
    fn default() -> Self {
        Self::Arrive
    }
}

impl Topic for Sidequest1 {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Arrive => DialogueData::new(
                vec![
                    ("You landed on the astroid Patricia wanted.", None),
                ],
                vec![(
                    "Collect sample for patricia.",
                    Some(NextDialogue::Next(Self::Collected)),
                )],
            ),
            Self::Collected => DialogueData::new(
                vec![(
                    "Sample retrieved. Not sure how  this helps us go back home.",
                    None,
                )],
                vec![("[Return]", Some(NextDialogue::End))],
            ),
        }
    }
}

pub fn handle_mission(
    mut chosen_evs: EventReader<ChosenEvent<Sidequest1>>,
    mut time: ResMut<StationTime>,
    mut state: ResMut<PatriciaState>,
    mission: Res<CurrentMission>,
    mut trust: Query<&mut Trust, With<Patricia>>
) {
    for ev in chosen_evs.iter() {
        match ev.current {
            Sidequest1::Arrive => {
                add_seconds(&mut time, mission.0.mission_time() as f64);
            }
            Sidequest1::Collected => {
                state.first_sidequest = SidequestState::Finished;
                if let Ok(mut trust) = trust.get_single_mut() {
                    trust.0 += 0.2;
                }
            }
        }
    }
}
