use crate::dialogue::DialogueData;
use crate::encounter::CancelMission;
use crate::time::{add_seconds, StationTime};
use crate::topic::{ChosenEvent, NextDialogue, Topic};

use bevy::prelude::*;
use chrono::Duration as ChronoDuration;

#[derive(Clone)]
pub enum ShipBreaks {
    Question,
    FixIt,
}

impl Default for ShipBreaks {
    fn default() -> Self {
        Self::Question
    }
}

impl Topic for ShipBreaks {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Question => DialogueData::new(
                vec![("On your way, space dust has damaged your engine.", None),
                    ("It still works, but it won't last for the entire mission.", None),
                    ("You can try fix it here, but it will take some time.", None),
                ],
                vec![
                    ("Spend time alone and fix your engine.", Some(NextDialogue::Next(Self::FixIt))),
                    ("Return to the ship for a proper repair.", Some(NextDialogue::End))
                ],
            ),
            Self::FixIt => DialogueData::new(
                vec![("After an hour you successfully fix the engine.", None)],
                vec![("Great!", Some(NextDialogue::End))],
            ),
        }
    }
}

pub fn handle(
    mut chosen_evs: EventReader<ChosenEvent<ShipBreaks>>,
    mut cancel: ResMut<CancelMission>,
    mut time: ResMut<StationTime>,
) {
    for ev in chosen_evs.iter() {
        if ev.chosen == 0 {
            let elapsed = ChronoDuration::weeks(52).num_seconds() as u32
                / time.multiplier;

            add_seconds(&mut time, elapsed as f64);
        } else if ev.chosen == 1 {
            cancel.0 = true;
        }
    }
}
