use crate::dialogue::DialogueData;
use crate::encounter::CancelMission;
use crate::time::{add_seconds, StationTime};
use crate::topic::{ChosenEvent, NextDialogue, Topic};

use bevy::prelude::*;
use chrono::Duration as ChronoDuration;

#[derive(Clone)]
pub enum ShipSOS {
    RadioSounds,
    TuneFrequency,
    Ignore,
    Follow,
}

impl Default for ShipSOS {
    fn default() -> Self {
        Self::RadioSounds
    }
}

impl Topic for ShipSOS {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::RadioSounds => DialogueData::new(
                vec![("The radio is making some strange noises.", None)],
                vec![
                    ("Tune the radio", Some(NextDialogue::Next(Self::TuneFrequency))),
                    ("Ignore it", Some(NextDialogue::Next(Self::Ignore)))
                ],
            ),
            Self::TuneFrequency => DialogueData::new(
                vec![("You tune the radio and hear some beeping sounds.", None),
                ("[BEEP BEEP BEEP]", None),
                ("[BEEEEEEP BEEEEEEP BEEEEEEP]", None),
                ("[BEEP BEEP BEEP]", None),
                ("It says SOS in Morse Code.", None),
                ("Could be worrying.", None)],
                vec![
                    (
                        "Determine the source of the signal and approach",
                        Some(NextDialogue::Next(Self::Follow))
                    ),
                    ("Ignore it", Some(NextDialogue::Next(Self::Ignore)))
                ],
            ),
            Self::Ignore => DialogueData::new(
                vec![("You ignore it and continue with your mission.", None)],
                vec![("Continue", Some(NextDialogue::End))],
            ),
            Self::Follow => DialogueData::new(
                vec![
                    ("You slowly approach the source of the signal.", None),
                    ("As you get closer, your radio gets louder.", None),
                    ("Slowly, you realize the source of the sound was none other than... your ship.", None),
                    ("Upon boarding you see Nora pressing random buttons on your control panel.", None),
                    ],
                vec![("Hands off, Nora!", Some(NextDialogue::End))],
            ),
        }
    }
}

pub fn handle(
    mut chosen_evs: EventReader<ChosenEvent<ShipSOS>>,
    mut cancel: ResMut<CancelMission>,
    mut time: ResMut<StationTime>,
) {
    for ev in chosen_evs.iter() {
        if matches!(ev.current, ShipSOS::TuneFrequency) {
            if ev.chosen == 0 {
                let elapsed = ChronoDuration::weeks(30).num_seconds() as u32
                    / time.multiplier;

                add_seconds(&mut time, elapsed as f64);
                cancel.0 = true;
            }
        }
    }
}
