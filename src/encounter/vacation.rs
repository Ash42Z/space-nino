use crate::dialogue::DialogueData;
use crate::topic::{ChosenEvent, NextDialogue, Topic};
use crate::time::{add_seconds, StationTime};

use chrono::Duration as ChronoDuration;
use bevy::prelude::*;

#[derive(Clone)]
pub enum Vacation {
    Question,
    DayOff,
    Continue,
    AfterRest,
}

impl Default for Vacation {
    fn default() -> Self {
        Self::Question
    }
}

impl Topic for Vacation {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Question => DialogueData::new(
                vec![
                    ("On your way down you stop for a while to think.", None),
                    ("You realize you haven't had time for yourself in a while.", None),
                    ("The others only see you now and then, but it's been nonstop work for you.", None),
                    ("Should you maybe take some time off, to keep your sanity?", None)
                    ],
                vec![
                    ("Yes, I'll take a day off", Some(NextDialogue::Next(Self::DayOff))),
                    ("No, the mission is too important", Some(NextDialogue::Next(Self::Continue)))
                ],
            ),
            Self::DayOff => DialogueData::new(
                vec![
                    ("You turn off the engines of the landing pod,", None),
                    ("take off your seatbelt,", None),
                    ("and lie down, closing your eyes.", None),
                    ("...", None),
                    ("How long has it been since you last had a good rest?", None),
                    ("Nowadays, everyone seems in such a hurry.", None),
                    ("...", None),
                    ("Except for you, that is.", None),
                    ("You would like nothing less than to get a good, long, rest.", None),
                    ("...", None),
                    ("Your friends can wait.", None),
                    ("You deserve this.", None),
                    ],
                    
                vec![
                    ("Rest", Some(NextDialogue::Next(Self::AfterRest)))
                ]
            ),
            Self::Continue => DialogueData::new(
                vec![("You are a little exhausted, but you decide to continue.", None),
                ("Your friends come first.", None),
                ("This is your responsibility.", None)
                ],
                vec![("Continue", Some(NextDialogue::End))]
            ),
            Self::AfterRest => DialogueData::new(
                vec![
                    ("...", None),
                    ("......", None),
                    ("You wake up.", None),
                    ("You feel fresh and ready to go.", None),
                ],
                vec![("Continue mission", Some(NextDialogue::End))]
            ),
        }
    }
}

pub fn handle(
    mut chosen_evs: EventReader<ChosenEvent<Vacation>>,
    mut time: ResMut<StationTime>
) {
    for ev in chosen_evs.iter() {
        if matches!(ev.current, Vacation::DayOff) {
            let elapsed = ChronoDuration::weeks(200).num_seconds() as u32
                / time.multiplier;

            add_seconds(&mut time, elapsed as f64);
        }
    }
}
