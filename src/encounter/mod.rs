use crate::mission::Mission;
use crate::topic::{Topic, TopicPlugin};
use crate::GameView;

use bevy::prelude::*;

mod ship_breaks;
use ship_breaks::{handle as ship_breaks_handle, ShipBreaks};
mod radiation_burst;
use radiation_burst::{handle as radiation_burst_handle, RadiationBurst};
mod ship_sos;
use ship_sos::{handle as ship_sos_handle, ShipSOS};
mod fake_danger;
use fake_danger::{handle as fake_danger_handle, FakeDanger};
mod vacation;
use vacation::{handle as vacation_handle, Vacation};

pub struct EncounterPlugin;

impl Plugin for EncounterPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(setup)
            .add_plugin(TopicPlugin::<ShipBreaks>::default())
            .add_system(ship_breaks_handle)
            .add_plugin(TopicPlugin::<RadiationBurst>::default())
            .add_system(radiation_burst_handle)
            .add_plugin(TopicPlugin::<ShipSOS>::default())
            .add_system(ship_sos_handle)
            .add_plugin(TopicPlugin::<FakeDanger>::default())
            .add_system(fake_danger_handle)
            .add_plugin(TopicPlugin::<Vacation>::default())
            .add_system(vacation_handle);
    }
}

#[derive(Resource)]
pub struct CurrentEncounter(pub Encounter);

#[derive(Resource)]
pub struct CancelMission(pub bool);

fn setup(mut commands: Commands) {
    commands.insert_resource(CancelMission(false));
}

#[derive(Clone, PartialEq, Eq, Debug, Hash)]
pub enum Encounter {
    ShipBreaks(u32),
    RadiationBurst(u32),
    ShipSOS(u32),
    FakeDanger(u32),
    Vacation(u32),
}

impl Encounter {
    pub fn multiplier(&self) -> u32 {
        match self {
            Self::ShipBreaks(mult)
            | Self::RadiationBurst(mult)
            | Self::ShipSOS(mult)
            | Self::FakeDanger(mult)
            | Self::Vacation(mult) => *mult,
        }
    }

    pub fn with_multiplier(&self, m: u32) -> Self {
        match self {
            Self::ShipBreaks(_) => Self::ShipBreaks(m / 2),
            Self::RadiationBurst(_) => Self::RadiationBurst(m / 2),
            Self::ShipSOS(_) => Self::ShipSOS(m / 10),
            Self::FakeDanger(_) => Self::FakeDanger(m / 10),
            Self::Vacation(_) => Self::Vacation(m / 2),
        }
    }

    pub fn spawn(&self, mission: Mission, commands: &mut Commands) {
        commands.insert_resource(CurrentEncounter(self.clone()));

        match self {
            Self::ShipBreaks(_) => {
                ShipBreaks::spawn(commands, GameView::AfterEncounter(mission));
            }
            Self::RadiationBurst(_) => {
                RadiationBurst::spawn(
                    commands,
                    GameView::AfterEncounter(mission),
                );
            }
            Self::ShipSOS(_) => {
                ShipSOS::spawn(commands, GameView::AfterEncounter(mission));
            }
            Self::FakeDanger(_) => {
                FakeDanger::spawn(commands, GameView::AfterEncounter(mission));
            }
            Self::Vacation(_) => {
                Vacation::spawn(commands, GameView::AfterEncounter(mission));
            }
        }
    }
}
