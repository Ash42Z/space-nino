use crate::dialogue::DialogueData;
use crate::encounter::CancelMission;
use crate::topic::{ChosenEvent, NextDialogue, Topic};

use bevy::prelude::*;

#[derive(Clone)]
pub enum FakeDanger {
    CallFromShip,
    Ignore,
    Stop,
    Answer,
    GoBack,
}

impl Default for FakeDanger {
    fn default() -> Self {
        Self::CallFromShip
    }
}

impl Topic for FakeDanger {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::CallFromShip => DialogueData::new(
                vec![("On your way you hear a ringing sound.", None),
                     ("Its an incoming message from the ship.", None)],
                vec![
                    ("Play message", Some(NextDialogue::Next(Self::Answer))),
                    ("Ignore", Some(NextDialogue::Next(Self::Ignore)))
                ],
            ),
            Self::Ignore => DialogueData::new(
                vec![("It keeps ringing.", None)],
                vec![
                    ("Play message", Some(NextDialogue::Next(Self::Answer))),
                    ("Keep Ignoring", Some(NextDialogue::Next(Self::Stop)))
                ]
            ),
            Self::Stop => DialogueData::new(
                vec![("The ringing finally stops.", None)],
                vec![("Continue", Some(NextDialogue::End))]
            ),
            Self::Answer => DialogueData::new(
                vec![("You hear Nicholas on the other side.", None),
                ("HELP!!", Some("Nicholas")),
                ("STANLEY IS TRYING TO KILL ME!!!!", Some("Nicholas")),
                ("HURRY!!!", Some("Nicholas")),
                ("The message suddenly ends.", None),],
                vec![
                    ("Go back to the ship", Some(NextDialogue::Next(Self::GoBack))),
                    ("Continue with your mission", Some(NextDialogue::End))
                ],
            ),
            Self::GoBack => DialogueData::new(
                vec![
                    ("You fly back to the ship", None),
                    ("Everything seems normal when you get back on board.", None),
                    ("Turns out Stanley never tried to kill Nicholas.", None),
                    ("Nicholas made it up just because he wanted to see you again.", None),
                ],
                vec![
                    ("Oh, Nicholas...", Some(NextDialogue::End))
                ],
            ),
        }
    }
}

pub fn handle(
    mut chosen_evs: EventReader<ChosenEvent<FakeDanger>>,
    mut cancel: ResMut<CancelMission>,
) {
    for ev in chosen_evs.iter() {
        if matches!(ev.current, FakeDanger::GoBack) {
            cancel.0 = true;
        }
    }
}
