use crate::dialogue::DialogueData;
use crate::encounter::CancelMission;
use crate::time::{add_seconds, StationTime};
use crate::topic::{ChosenEvent, NextDialogue, Topic};

use bevy::prelude::*;
use chrono::Duration as ChronoDuration;

use rand::prelude::*;

#[derive(Clone)]
pub enum RadiationBurst {
    Question,
    Success,
    Fail,
}

impl Default for RadiationBurst {
    fn default() -> Self {
        Self::Question
    }
}

impl Topic for RadiationBurst {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Question => DialogueData::new(
                vec![
                ("Suddenly, a radiation burst disrupts your navigation system.", None),
                ("You could try to navigate on your own, but it's very difficult.", None),
                ("You could also go back, out of the scope of the burst, then back to the ship.", None),
                ("Do you take your chances and continue?", None)],
                vec![
                    ("Go back to the ship", Some(NextDialogue::End)),
                    ("Navigate on your own", None)
                ],
            ),
            Self::Success => DialogueData::new(
                vec![("After a short time the navigation system comes back online.", None)],
                vec![("Splendid!", Some(NextDialogue::End))],
            ),
            Self::Fail => DialogueData::new(
                vec![("After two hours you realize you're lost.", None),
                     ("You go back to the ship.", None)],
                vec![("Bummer.", Some(NextDialogue::End))],
            ),
        }
    }
}

pub fn handle(
    mut chosen_evs: EventReader<ChosenEvent<RadiationBurst>>,
    mut cancel: ResMut<CancelMission>,
    mut time: ResMut<StationTime>,
    mut next_wr: EventWriter<NextDialogue<RadiationBurst>>,
) {
    for ev in chosen_evs.iter() {
        if matches!(ev.current, RadiationBurst::Question) {
            if ev.chosen == 0 {
                cancel.0 = true;
            } else if ev.chosen == 1 {
                let mut rng = rand::thread_rng();
                let rand: f64 = rng.gen();
                if rand > 0.5 {
                    let elapsed = ChronoDuration::weeks(10).num_seconds()
                        as u32
                        / time.multiplier;

                    add_seconds(&mut time, elapsed as f64);
                    next_wr.send(NextDialogue::Next(RadiationBurst::Success));
                } else {
                    let elapsed = ChronoDuration::weeks(80).num_seconds()
                        as u32
                        / time.multiplier;
                    add_seconds(&mut time, elapsed as f64);

                    next_wr.send(NextDialogue::Next(RadiationBurst::Fail));
                    cancel.0 = true;
                }
            }
        }
    }
}
