use crate::encounter::Encounter;
use crate::gamestate::{MissionCount, SidequestState};
use crate::patricia;
use crate::patricia::PatriciaState;
use crate::topic::Topic;
use crate::GameView;

use bevy::prelude::*;
use chrono::Duration as ChronoDuration;
use rand::Rng;

mod get_fuel;
use get_fuel::{GetFuel, GetFuelPlugin};

mod debug_mission;
use debug_mission::{DebugMission, DebugMissionPlugin};

mod get_nav;
use get_nav::{GetNavData, GetNavDataPlugin};

pub struct MissionPlugin;

impl Plugin for MissionPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(setup)
            .add_system(update_available_missions)
            .add_plugin(GetFuelPlugin)
            .add_plugin(DebugMissionPlugin)
            .add_plugin(GetNavDataPlugin);
    }
}

#[derive(Resource)]
pub struct CurrentMission(pub Mission);

#[derive(Resource)]
pub struct AvailableEncounters(pub Vec<Encounter>);

fn setup(mut commands: Commands) {
    commands.insert_resource(AvailableMissions(vec![]));
    commands.insert_resource(AvailableEncounters(vec![
        Encounter::ShipBreaks(0),
        Encounter::RadiationBurst(0),
        Encounter::ShipSOS(0),
        Encounter::FakeDanger(0),
        Encounter::Vacation(0),
    ]));
}

#[derive(Hash, Debug, Clone, Eq, PartialEq, Copy)]
pub enum NavDataAmount {
    Low,
    Medium,
    High,
}

impl NavDataAmount {
    pub fn string(&self) -> &'static str {
        match self {
            Self::Low => "low",
            Self::Medium => "medium",
            Self::High => "high",
        }
    }

    pub fn value(&self) -> u32 {
        match self {
            Self::Low => 1,
            Self::Medium => 2,
            Self::High => 3,
        }
    }

    fn rand() -> Self {
        let mut rng = rand::thread_rng();
        match rng.gen_range(0..3) {
            0 => Self::Low,
            1 => Self::Medium,
            _ => Self::High,
        }
    }
}

#[derive(Hash, Debug, Clone, Eq, PartialEq)]
pub enum Mission {
    GetFuel {
        fuel: u32,
        multiplier: u32,
        launch_time: ChronoDuration,
        collection_time: ChronoDuration,
    },
    GetNavData {
        nav_data: NavDataAmount,
        multiplier: u32,
        launch_time: ChronoDuration,
        collection_time: ChronoDuration,
    },
    #[allow(dead_code)]
    DebugMission,
    PatriciaSideQuest0,
    PatriciaSideQuest1,
    PatriciaSideQuest2,
}

pub fn seconds_to_get_there(mult: u32, before: u32) -> u32 {
    let max = std::cmp::max(mult, before);
    let min = std::cmp::min(mult, before);

    (max - min) / 8000
}

impl Mission {
    pub fn name(&self) -> String {
        match self {
            Self::GetFuel { fuel, .. } => {
                format!("Acquire fuel materials\n(+{} fuel cells)", fuel)
            }
            Self::GetNavData { nav_data, .. } => {
                format!(
                    "Acquire navigational data\n({} quality)",
                    nav_data.string()
                )
            },
            Self::PatriciaSideQuest0 => "Listen to Patricia and acquite navigational data (low quality)".into(),
            Self::PatriciaSideQuest1 => "Investigate astroid for Patricia".into(),
            Self::PatriciaSideQuest2 => "Collect ice samples for Patricia".into(),

            Self::DebugMission => {
                "A super far away asteroid with a cornucopia of fuel and nav data potential.".to_string()
            }
        }
    }

    pub fn multiplier(&self) -> u32 {
        match self {
            Self::GetFuel { multiplier, .. }
            | Self::GetNavData { multiplier, .. } => *multiplier,
            Self::PatriciaSideQuest0
            | Self::PatriciaSideQuest1
            | Self::PatriciaSideQuest2 => 10,
            Self::DebugMission => 52,
        }
    }

    pub fn mission_time(&self) -> u32 {
        match self {
            Self::GetFuel {
                collection_time, ..
            }
            | Self::GetNavData {
                collection_time, ..
            } => collection_time.num_seconds() as u32,
            Self::PatriciaSideQuest0
            | Self::PatriciaSideQuest1
            | Self::PatriciaSideQuest2 => 10,
            Self::DebugMission => 30844800,
        }
    }

    pub fn launch_time(&self) -> u32 {
        match self {
            Self::GetFuel { launch_time, .. }
            | Self::GetNavData { launch_time, .. } => {
                launch_time.num_seconds() as u32
            }
            Self::PatriciaSideQuest0
            | Self::PatriciaSideQuest1
            | Self::PatriciaSideQuest2 => 10,
            Self::DebugMission => 0,
        }
    }

    pub fn spawn(&self, commands: &mut Commands) {
        commands.insert_resource(CurrentMission(self.clone()));

        match self {
            Self::GetFuel { .. } => {
                GetFuel::spawn(commands, GameView::AfterMission);
            }
            Self::GetNavData { .. } => {
                GetNavData::spawn(commands, GameView::AfterMission);
            }
            Self::DebugMission => {
                DebugMission::spawn(commands, GameView::AfterMission);
            }
            Self::PatriciaSideQuest0 => {
                patricia::side_quest_0::Sidequest0::spawn(
                    commands,
                    GameView::AfterMission,
                );
            }
            Self::PatriciaSideQuest1 => {
                patricia::side_quest_1::Sidequest1::spawn(
                    commands,
                    GameView::AfterMission,
                );
            }
            Self::PatriciaSideQuest2 => {
                patricia::side_quest_2::Sidequest2::spawn(
                    commands,
                    GameView::AfterMission,
                );
            }
        }
    }
}

#[derive(Resource)]
pub struct AvailableMissions(pub Vec<(Mission, Option<Encounter>)>);

fn update_available_missions(
    mut available: ResMut<AvailableMissions>,
    patricia_state: Res<PatriciaState>,
    mission_count: Res<MissionCount>,
    encounters: Res<AvailableEncounters>,
) {
    if mission_count.is_changed() || patricia_state.is_changed() {
        let mut new_available: Vec<(Mission, Option<Encounter>)> = vec![];

        let mut rng = rand::thread_rng();

        let encounter = encounters.0.get(0);

        let fuel_total_time = ChronoDuration::weeks(rng.gen_range(52..200));
        let fuel_mult = rng.gen_range(10..400000);
        let collection_time = fuel_total_time.num_seconds() / fuel_mult;
        let fuel_encounter = 
            encounter.filter(|_| rng.gen::<bool>())
            .map(|e| e.with_multiplier(fuel_mult as u32));

        new_available.push((
            Mission::GetFuel {
                fuel: rng.gen_range(3..7),
                multiplier: fuel_mult as u32,
                launch_time: ChronoDuration::days(rng.gen_range(1..600)),
                collection_time: ChronoDuration::seconds(collection_time),
            },
            fuel_encounter,
        ));

        let nav_total_time = ChronoDuration::weeks(rng.gen_range(52..200));
        let nav_mult = rng.gen_range(10..400000);
        let collection_time = nav_total_time.num_seconds() / nav_mult;
        let nav_encounter = 
            encounter.filter(|_| rng.gen::<bool>())
            .map(|e| e.with_multiplier(nav_mult as u32));

        new_available.push((
            Mission::GetNavData {
                nav_data: NavDataAmount::rand(),
                multiplier: fuel_mult as u32,
                launch_time: ChronoDuration::days(rng.gen_range(1..600)),
                collection_time: ChronoDuration::seconds(collection_time),
            },
            nav_encounter,
        ));

        if patricia_state.zeroth_sidequest == SidequestState::Given {
            new_available.push((Mission::PatriciaSideQuest0, None));
        }
        if patricia_state.first_sidequest == SidequestState::Given {
            new_available.push((Mission::PatriciaSideQuest1, None));
        }
        if patricia_state.second_sidequest == SidequestState::Given {
            new_available.push((Mission::PatriciaSideQuest2, None));
        }

        available.0 = new_available;
    }
}
