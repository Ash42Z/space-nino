use crate::dialogue::DialogueData;
use crate::gamestate::{Fuel, NavData};
use crate::mission::{CurrentMission};
use crate::time::{add_seconds, StationTime};
use crate::topic::{ChosenEvent, NextDialogue, Topic, TopicPlugin};

use bevy::prelude::*;
use iyes_loopless::prelude::*;

pub struct DebugMissionPlugin;

impl Plugin for DebugMissionPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(TopicPlugin::<DebugMission>::default())
            .add_system(handle.run_if_resource_exists::<CurrentMission>());
    }
}

#[derive(Clone)]
pub enum DebugMission {
    Arrive,
    Collected,
}

impl Default for DebugMission {
    fn default() -> Self {
        Self::Arrive
    }
}

impl Topic for DebugMission {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Arrive => DialogueData::new(
                vec![("You arrived on the asteroid.", None),
                ("Literally nothing interesting exists here.", None)],
                vec![(
                    "Pretend to collect materials.",
                    Some(NextDialogue::Next(Self::Collected)),
                )],
            ),
            Self::Collected => DialogueData::new(
                vec![("This small detour cost you fifty one years.", None)],
                vec![("Go home", Some(NextDialogue::End))],
            ),
        }
    }
}

pub fn handle(
    mut chosen_evs: EventReader<ChosenEvent<DebugMission>>,
    mut fuel: ResMut<Fuel>,
    mut nav_data: ResMut<NavData>,
    mut time: ResMut<StationTime>,
) {
    for ev in chosen_evs.iter() {
        if matches!(ev.current, DebugMission::Collected) {
            fuel.0 += 15;
            nav_data.0 += 10;
            add_seconds(&mut time, 30844800 as f64);
        }
    }
}
