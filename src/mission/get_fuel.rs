use crate::dialogue::DialogueData;
use crate::gamestate::Fuel;
use crate::mission::{CurrentMission, Mission};
use crate::time::{add_seconds, StationTime};
use crate::topic::{ChosenEvent, NextDialogue, Topic, TopicPlugin};

use bevy::prelude::*;
use iyes_loopless::prelude::*;

pub struct GetFuelPlugin;

impl Plugin for GetFuelPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(TopicPlugin::<GetFuel>::default())
            .add_system(handle.run_if_resource_exists::<CurrentMission>());
    }
}

#[derive(Clone)]
pub enum GetFuel {
    Message,
    Found,
}

impl Default for GetFuel {
    fn default() -> Self {
        Self::Message
    }
}

impl Topic for GetFuel {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Message => DialogueData::new(
                vec![("You landed on the astroid", None)],
                vec![(
                    "Collect materials",
                    Some(NextDialogue::Next(Self::Found)),
                )],
            ),
            Self::Found => DialogueData::new(
                vec![("Found materials for fuel cells", None)],
                vec![("Return to station", Some(NextDialogue::End))],
            ),
        }
    }
}

pub fn handle(
    mut chosen_evs: EventReader<ChosenEvent<GetFuel>>,
    mission: Res<CurrentMission>,
    mut fuel: ResMut<Fuel>,
    mut time: ResMut<StationTime>,
) {
    for ev in chosen_evs.iter() {
        if matches!(ev.current, GetFuel::Found) {
            if let Mission::GetFuel {
                fuel: amount,
                collection_time,
                ..
            } = mission.0 {
                fuel.0 += amount;
                add_seconds(&mut time, collection_time.num_seconds() as f64);
            }
        }
    }
}
