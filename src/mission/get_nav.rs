use crate::dialogue::DialogueData;
use crate::gamestate::NavData;
use crate::mission::{CurrentMission, Mission};
use crate::time::{add_seconds, StationTime};
use crate::topic::{ChosenEvent, NextDialogue, Topic, TopicPlugin};

use bevy::prelude::*;
use iyes_loopless::prelude::*;

pub struct GetNavDataPlugin;

impl Plugin for GetNavDataPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(TopicPlugin::<GetNavData>::default())
            .add_system(handle.run_if_resource_exists::<CurrentMission>());
    }
}

#[derive(Clone)]
pub enum GetNavData {
    Message,
    Found,
}

impl Default for GetNavData {
    fn default() -> Self {
        Self::Message
    }
}

impl Topic for GetNavData {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Message => DialogueData::new(
                vec![("You arrived at the correct orbit for testing", None)],
                vec![(
                    "Activate sensors",
                    Some(NextDialogue::Next(Self::Found)),
                )],
            ),
            Self::Found => DialogueData::new(
                vec![("Aquired navigational data", None)],
                vec![("Return to station", Some(NextDialogue::End))],
            ),
        }
    }
}

pub fn handle(
    mut chosen_evs: EventReader<ChosenEvent<GetNavData>>,
    mission: Res<CurrentMission>,
    mut nav: ResMut<NavData>,
    mut time: ResMut<StationTime>,
) {
    for ev in chosen_evs.iter() {
        if matches!(ev.current, GetNavData::Found) {
            if let Mission::GetNavData {
                nav_data: amount,
                collection_time,
                ..
            } = mission.0
            {
                nav.0 += amount.value();
                add_seconds(&mut time, collection_time.num_seconds() as f64)
            }
        }
    }
}
