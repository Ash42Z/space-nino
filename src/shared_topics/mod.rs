use crate::topic::{Topic, TopicPlugin};

use bevy::prelude::*;
use crate::{GameView, BackgroundSprites};

use crate::utils::spawn_lounge_scene;

mod after_first;
pub use after_first::{AfterFirstMission, handle_after_first};

pub struct SharedTopicsPlugin;

#[derive(Resource, Default)]
pub struct SharedTopicsState {
    done_after_first: bool,
}

impl Plugin for SharedTopicsPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(spawn)
            .add_plugin(TopicPlugin::<AfterFirstMission>::default())
            .add_system(handle_after_first);
    }
}

fn spawn(mut commands: Commands) {
    commands.insert_resource(SharedTopicsState::default());
}

pub fn spawn_topic_on_return(
    commands: &mut Commands,
    state: &SharedTopicsState,
    mission_count: u32,
    backgrounds: &Res<BackgroundSprites>,
    characters: Vec<Handle<Image>>,
    scene: (Entity, &mut Handle<Image>),
    ) -> bool {
    
    if mission_count == 1 && !state.done_after_first {
        spawn_lounge_scene(commands, backgrounds, characters, scene);
        AfterFirstMission::spawn(commands, GameView::Spacestation);
        true
    } else {
        false
    }
}