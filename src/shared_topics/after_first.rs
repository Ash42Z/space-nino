use bevy::prelude::*;
use crate::topic::{Topic, ChosenEvent, NextDialogue};
use crate::shared_topics::SharedTopicsState;
use crate::dialogue::DialogueData;
use crate::time::DisplayTimeInfo;

#[derive(Clone)]
pub enum AfterFirstMission {
    Start,
    Ruckus,
    FewSeconds,
    BlackHole,
    TimeDifference
}

impl Default for AfterFirstMission {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for AfterFirstMission {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![("YOU!", Some("Nora")),
                     ("Hey, you're back!", Some("Nicholas")),
                     ("YOU!!!", Some("Nora")),
                     ("I thought you left us all to die.", Some("Stanley")),
                     ("WHO DO YOU THINK YOU ARE?!", Some("Nora"))
                ],
                vec![
                    (
                        "What's all this ruckus about?",
                        Some(NextDialogue::Next(Self::Ruckus)),
                    ),
                    (
                        "I was just gone for a few minutes.",
                        Some(NextDialogue::Next(Self::FewSeconds)),
                    ),
                ],
            ),
            Self::Ruckus => DialogueData::new(
                vec![("All this ruckus?", Some("Nora")),
                     ("ALL THIS RUCKUS???", Some("Nora"))],
                vec![
                    (
                        "...Yeah?",
                        Some(NextDialogue::Next(Self::BlackHole)),
                    )
                ],
            ),
            Self::FewSeconds => DialogueData::new(
                vec![("Just a few minutes?", Some("Nora")),
                     ("JUST A FEW MINUTES???", Some("Nora"))],
                vec![
                    (
                        "...Yeah?",
                        Some(NextDialogue::Next(Self::BlackHole)),
                    )
                ],
            ),
            Self::BlackHole => DialogueData::new(
                vec![("YOU WERE GONE FOR A MONTH!!", Some("Nora")),
                    ("* mumbles to herself *", Some("Patricia")),
                     ("(a month... a few minutes...)", Some("Patricia")),
                     ("Oh! Of course!", Some("Patricia")),
                     ("What?", Some("Nora")),
                     ("It's so obvious!", Some("Patricia")),
                     ("What??", Some("Nicholas")),
                     ("How did we not see this before?", Some("Patricia")),
                     ("SPIT IT OUT ALREADY", Some("Stanley")),
                     ("It's the Black Hole!", Some("Patricia"))],
                vec![
                    (
                        "...what.",
                        Some(NextDialogue::Next(Self::TimeDifference)),
                    ),
                    (
                        "What?",
                        Some(NextDialogue::Next(Self::TimeDifference)),
                    )
                ],
            ),
            Self::TimeDifference => DialogueData::new(
                vec![("Time works differently around Black Holes.", Some("Patricia")),
                     ("The closer you are to the Black Hole,", Some("Patricia")),
                     ("The slower time moves for you,", Some("Patricia")),
                     ("And the faster it goes for us.", Some("Patricia")),
                     ("If you go down below - even for a minute!", Some("Patricia")),
                     ("We might be waiting years to see you come back.", Some("Patricia")),
                     ("Years!", Some("Patricia")),
                     ("YEARS?!!", Some("Nora")),
                     ("Ugh...", Some("Stanley")),
                     ("It's trivial if you know a bit of General Relativity.", Some("Patricia")),
                     ("Then it's lucky I don't.", Some("Stanley")),
                     ("But what does this mean for us?", Some("Nora")),
                     ("It means...", Some("Patricia")),
                     ("We have less time than we thought.", Some("Patricia")),
                     ("I knew this trip would be the death of me.", Some("Stanley")),
                     ("I knew it.", Some("Stanley"))],
                vec![("Return", Some(NextDialogue::End))],
            ),
        }
    }
}

pub fn handle_after_first(
    mut option_evs: EventReader<ChosenEvent<AfterFirstMission>>,
    mut state: ResMut<SharedTopicsState>,
    mut time_display: ResMut<DisplayTimeInfo>
) {
    for ev in option_evs.iter() {
        if matches!(ev.current, AfterFirstMission::TimeDifference) {
            state.done_after_first = true;
            time_display.0 = true;
        }
    }
}
