use crate::dialogue::DialogueData;
use crate::gamestate::ConversationIndex;
use crate::topic::{ChosenEvent, NextDialogue, Topic, TopicPlugin};

use bevy::prelude::*;

use super::Nora;

pub struct GenericPlugin;

impl Plugin for GenericPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(TopicPlugin::<Initial>::default())
            .add_system(handle_initial)
            .add_plugin(TopicPlugin::<RepeatResponse>::default());
    }
}

#[derive(Clone)]
pub enum Initial {
    Start
}

impl Default for Initial {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for Initial {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![
                    ("Did I tell you about my son?", Some("Nora")),
                    ("His name is Joshua.", Some("Nora")),
                    ("He's four years old, and the sweetest soul you'll meet.", Some("Nora")),
                    ("He once woke in the middle of the night", Some("Nora")),
                    ("Just cause a butterfly flew into his room", Some("Nora")),
                    ("Landing on his face.", Some("Nora")),
                    ("He caught it with his bare hands,", Some("Nora")),
                    ("Carried it outside,", Some("Nora")),
                    ("And set it free.", Some("Nora")),
                    ("I really miss him.", Some("Nora"))
                ],
                vec![
                    ("[Return]", Some(NextDialogue::End))
                ],
            )
        }
    }
}

fn handle_initial(
    mut option_evs: EventReader<ChosenEvent<Initial>>,
    mut conversation: Query<&mut ConversationIndex, With<Nora>>
) {
    let mut conversation = conversation.get_single_mut().unwrap();

    for ev in option_evs.iter() {
        if matches!(ev.current, Initial::Start) {
            conversation.0 += 1;
        }
    }
}

#[derive(Clone)]
pub enum RepeatResponse {
    Start,
}

impl Default for RepeatResponse {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for RepeatResponse {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![
                    ("You see now why I care so much about getting back?", Some("Nora")),
                ],
                vec![
                    ("[Return]", Some(NextDialogue::End))
                ],
            ),
        }
    }
}
