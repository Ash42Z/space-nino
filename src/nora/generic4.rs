use crate::dialogue::DialogueData;
use crate::gamestate::ConversationIndex;
use crate::topic::{ChosenEvent, NextDialogue, Topic, TopicPlugin};

use bevy::prelude::*;

use super::Nora;

pub struct GenericPlugin;

impl Plugin for GenericPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(TopicPlugin::<Initial>::default())
            .add_system(handle_initial)
            .add_plugin(TopicPlugin::<RepeatResponse>::default());
    }
}

#[derive(Clone)]
pub enum Initial {
    Start
}

impl Default for Initial {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for Initial {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![
                    ("This is the worst place to be stuck in.", Some("Nora")),
                    ("And with the worst people.", Some("Nora")),
                    ("It's strange what being confined in a small space can do to you.", Some("Nora")),
                    ("I don't want to be here.", Some("Nora")),
                    ("I probably never wanted to be here.", Some("Nora")),
                    ("It's kinda your fault, isn't it?", Some("Nora")),
                    ("Well, maybe not entirely your fault.", Some("Nora")),
                    ("I don't know how spaceships work.", Some("Nora")),
                    ("But I don't like being here.", Some("Nora")),
                    ("Not one bit.", Some("Nora"))
                ],
                vec![
                    ("[Return]", Some(NextDialogue::End))
                ],
            )
        }
    }
}

fn handle_initial(
    mut option_evs: EventReader<ChosenEvent<Initial>>,
    mut conversation: Query<&mut ConversationIndex, With<Nora>>
) {
    let mut conversation = conversation.get_single_mut().unwrap();

    for ev in option_evs.iter() {
        if matches!(ev.current, Initial::Start) {
            conversation.0 += 1;
        }
    }
}

#[derive(Clone)]
pub enum RepeatResponse {
    Start,
}

impl Default for RepeatResponse {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for RepeatResponse {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![
                    ("I know I seem harsh sometimes.", Some("Nora")),
                    ("I don't hate things as much as I say I do.", Some("Nora")),
                    ("...", Some("Nora")),
                    ("But that doesn't mean nothing makes me feel uncomfortable.", Some("Nora")),
                    ("I'm allowed to feel uncomfortable.", Some("Nora")),                    
                ],
                vec![
                    ("[Return]", Some(NextDialogue::End))
                ],
            ),
        }
    }
}
