use bevy::prelude::*;

use crate::dialogue::DialogueData;
use crate::gamestate::ConversationIndex;
use crate::topic::{NextDialogue, Topic, TopicPlugin, ChosenEvent};

use super::Nora;

pub struct GenericPlugin;

impl Plugin for GenericPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(TopicPlugin::<Initial>::default())
            .add_system(handle_initial)
            .add_plugin(TopicPlugin::<RepeatResponse>::default());
    }
}

#[derive(Clone)]
pub enum Initial {
    Start,
    Next,
}

impl Default for Initial {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for Initial {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![("I can't be stuck here forever.", Some("Nora")),
                     ("You better do something about it.", Some("Nora"))],
                vec![
                    (
                        "I will, I will.",
                        Some(NextDialogue::Next(Self::Next)),
                    ),
                    (
                        "You have nothing to worry about.",
                        Some(NextDialogue::Next(Self::Next)),
                    ),
                ],
            ),
            Self::Next => DialogueData::new(
                vec![
                    (
                        "I can't die here.",
                        Some("Nora"),
                    ),
                    ("My family needs me back at home.", Some("Nora")),
                ],
                vec![("Return", Some(NextDialogue::End))],
            )
        }
    }
}

fn handle_initial(
    mut option_evs: EventReader<ChosenEvent<Initial>>,
    mut conversation: Query<&mut ConversationIndex, With<Nora>>
) {
    let mut conversation = conversation.get_single_mut().unwrap();

    for ev in option_evs.iter() {
        if matches!(ev.current, Initial::Next) {
            conversation.0 += 1;
        }
    }
}

#[derive(Clone)]
pub(crate) enum RepeatResponse {
    Start,
}

impl Default for RepeatResponse {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for RepeatResponse {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![("Don't you dare let me die.", Some("Nora"))],
                vec![
                    (
                        "Return",
                        Some(NextDialogue::End),
                    ),
                ],
            ),
        }
    }
}
