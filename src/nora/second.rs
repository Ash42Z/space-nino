use bevy::prelude::*;

use crate::dialogue::DialogueData;
use crate::gamestate::ConversationIndex;
use crate::topic::{NextDialogue, Topic, TopicPlugin, ChosenEvent};

use super::Nora;

pub struct GenericPlugin;

impl Plugin for GenericPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(TopicPlugin::<Initial>::default())
            .add_system(handle_initial)
            .add_plugin(TopicPlugin::<RepeatResponse>::default());
    }
}

#[derive(Clone)]
pub enum Initial {
    Start,
}

impl Default for Initial {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for Initial {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![("I have a family, you know.", Some("Nora")),
                     ("My son is waiting for me at home.", Some("Nora")),
                     ("He's only eleven.", Some("Nora")),
                     ("What will he do without me?", Some("Nora")),
                     ("I don't care about all that physics nonsense.", Some("Nora")),
                     ("Just fix the ship quick as you can.", Some("Nora")),
                     ("I have to get back.", Some("Nora")),
                     ("I have to.", Some("Nora"))],
                vec![("Return", Some(NextDialogue::End))],
            ),
        }
    }
}

fn handle_initial(
    mut option_evs: EventReader<ChosenEvent<Initial>>,
    mut conversation: Query<&mut ConversationIndex, With<Nora>>
) {
    let mut conversation = conversation.get_single_mut().unwrap();

    for ev in option_evs.iter() {
        if matches!(ev.current, Initial::Start) {
            conversation.0 += 1;
        }
    }
}

#[derive(Clone)]
pub(crate) enum RepeatResponse {
    Start,
}

impl Default for RepeatResponse {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for RepeatResponse {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![("How long will it take to fix this ship?", Some("Nora")),
                     ("I must get back home soon...", Some("Nora"))],
                vec![("Return", Some(NextDialogue::End))],
            ),
        }
    }
}
