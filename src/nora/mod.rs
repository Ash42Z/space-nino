use crate::gamestate::{
    Alive, CharacterBundle, CharacterName, RequestTopicEvent, Trust, Face, MissionCount, ConversationIndex,
};
use bevy::prelude::*;

use crate::topic::Topic;
use crate::GameView;

pub struct NoraPlugin;

mod initial;
use initial as Initial;

mod second;
use second as Second;

mod generic2;
mod generic3;
mod generic4;
mod generic5;
mod generic6;
mod generic7;
mod generic8;


impl Plugin for NoraPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(spawn)
            .add_system(dialogue)
            .add_event::<RequestTopicEvent<Nora>>()
            .add_plugin(Initial::GenericPlugin)
            .add_plugin(Second::GenericPlugin)
            .add_plugin(generic2::GenericPlugin)
            .add_plugin(generic3::GenericPlugin)
            .add_plugin(generic4::GenericPlugin)
            .add_plugin(generic5::GenericPlugin)
            .add_plugin(generic6::GenericPlugin)
            .add_plugin(generic7::GenericPlugin)
            .add_plugin(generic8::GenericPlugin);
    }
}

#[derive(Component)]
pub struct Nora;

#[derive(Resource, Default)]
pub struct NoraState {
}

fn spawn(mut commands: Commands, asset_server: Res<AssetServer>) {
    commands.spawn((
        CharacterBundle {
            name: CharacterName::new("Nora"),
            trust: Trust(0.5),
            image: asset_server.load("Nora1.png"),
            face: Face(asset_server.load("NoraFace1.png")),
            ..default()
        },
        Nora,
        Alive,
        ConversationIndex(0)
    ));
    commands.insert_resource(NoraState::default());
}

fn dialogue(
    mut commands: Commands,
    mut evs: EventReader<RequestTopicEvent<Nora>>,
    mission_count: Res<MissionCount>,
    conversation: Query<&ConversationIndex, With<Nora>>
) {
    if evs.iter().next().is_none() {
        return;
    }

    let conversation = conversation.get_single().unwrap();

    match (mission_count.0, conversation.0) {
        (0, 0) => Initial::Initial::spawn(&mut commands, GameView::Spacestation),
        (0, 1) => Initial::RepeatResponse::spawn(&mut commands, GameView::Spacestation),
        (1, 0) => Second::Initial::spawn(&mut commands, GameView::Spacestation),
        (1, 1) => Second::RepeatResponse::spawn(&mut commands, GameView::Spacestation),
        (2, 0) => generic2::Initial::spawn(&mut commands, GameView::Spacestation),
        (2, 1) => generic2::RepeatResponse::spawn(&mut commands, GameView::Spacestation),
        (3, 0) => generic3::Initial::spawn(&mut commands, GameView::Spacestation),
        (3, 1) => generic3::RepeatResponse::spawn(&mut commands, GameView::Spacestation),
        (4, 0) => generic4::Initial::spawn(&mut commands, GameView::Spacestation),
        (4, 1) => generic4::RepeatResponse::spawn(&mut commands, GameView::Spacestation),
        (5, 0) => generic5::Initial::spawn(&mut commands, GameView::Spacestation),
        (5, 1) => generic5::RepeatResponse::spawn(&mut commands, GameView::Spacestation),
        (6, 0) => generic6::Initial::spawn(&mut commands, GameView::Spacestation),
        (6, 1) => generic6::RepeatResponse::spawn(&mut commands, GameView::Spacestation),
        (7, 0) => generic7::Initial::spawn(&mut commands, GameView::Spacestation),
        (7, 1) => generic7::RepeatResponse::spawn(&mut commands, GameView::Spacestation),
        (8, 0) => generic8::Initial::spawn(&mut commands, GameView::Spacestation),
        (8, 1) => generic8::RepeatResponse::spawn(&mut commands, GameView::Spacestation),
        _ => {}
    }
}
