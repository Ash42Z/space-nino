use bevy::prelude::*;

use crate::dialogue::DialogueData;
use crate::gamestate::ConversationIndex;
use crate::topic::{NextDialogue, Topic, TopicPlugin, ChosenEvent};

use super::Nicholas;

pub struct GenericPlugin;

impl Plugin for GenericPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(TopicPlugin::<NicholasInitial>::default())
            .add_plugin(TopicPlugin::<RepeatResponse>::default())
            .add_system(handle_initial);
    }
}

#[derive(Clone)]
pub(crate) enum NicholasInitial {
    Start,
    AfterQuestion,
}

impl Default for NicholasInitial {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for NicholasInitial {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![("A black hole. Pretty bad luck, huh?", Some("Nicholas"))],
                vec![
                    (
                        "You don't know the half of it.",
                        Some(NextDialogue::Next(Self::AfterQuestion)),
                    ),
                    (
                        "I'm so sorry I took you on this trip.",
                        Some(NextDialogue::Next(Self::AfterQuestion)),
                    ),
                ],
            ),
            Self::AfterQuestion => DialogueData::new(
                vec![
                    (
                        "I'm not worried. I'm sure you can handle it.",
                        Some("Nicholas"),
                    ),
                    ("You're the best of the best.", Some("Nicholas")),
                ],
                vec![("Thanks!", Some(NextDialogue::End))],
            ),
        }
    }
}

fn handle_initial(
    mut option_evs: EventReader<ChosenEvent<NicholasInitial>>,
    mut conversation: Query<&mut ConversationIndex, With<Nicholas>>
) {
    let mut conversation = conversation.get_single_mut().unwrap();

    for ev in option_evs.iter() {
        if matches!(ev.current, NicholasInitial::AfterQuestion) {
            conversation.0 += 1;
        }
    }
}

#[derive(Clone)]
pub(crate) enum RepeatResponse {
    Start,
}

impl Default for RepeatResponse {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for RepeatResponse {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![("If worst comes to worst, at least you tried your best.", Some("Nicholas"))],
                vec![
                    (
                        "Return",
                        Some(NextDialogue::End),
                    ),
                ],
            ),
        }
    }
}
