use bevy::prelude::*;

use crate::dialogue::DialogueData;

use crate::gamestate::ConversationIndex;
use crate::topic::{NextDialogue, Topic, TopicPlugin, ChosenEvent};

use super::Nicholas;

pub struct GenericPlugin;

impl Plugin for GenericPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(TopicPlugin::<Initial>::default())
            .add_plugin(TopicPlugin::<RepeatResponse>::default())
            .add_system(handle_initial);
    }
}

#[derive(Clone)]
pub(crate) enum Initial {
    Start,
    AfterQuestion,
}

impl Default for Initial {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for Initial {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![("You were gone for so long!", Some("Nicholas")),
                     ("I don't want to be alone with these strangers.", Some("Nicholas")),
                     ("Do you have any idea how difficult the past month was for me?", Some("Nicholas")),
                     ("Ugh, with Stanley.", Some("Nicholas")),
                     ("He's so grumpy.", Some("Nicholas")),
                     ("And Nora.", Some("Nicholas")),
                     ("She's so *annoying*.", Some("Nicholas")),
                     ("All she talks about is her son.", Some("Nicholas")),
                     ("And Patricia!", Some("Nicholas")),
                     ("Well, I guess she's okay.", Some("Nicholas")),
                     ("But who knows what I'll think of her in a month.", Some("Nicholas")),
                     ("Or a year!", Some("Nicholas")),
                     ("That's too much for me, you know?", Some("Nicholas")),
                     ("...", Some("Nicholas")),
                     ("You're my best friend.", Some("Nicholas")),
                     ("Please, don't leave me alone again.", Some("Nicholas"))],
                vec![
                    (
                        "I'm sorry... You know there's no other way, I have to fix the ship.",
                        Some(NextDialogue::Next(Self::AfterQuestion)),
                    ),
                    (
                        "What else am I gonna do?",
                        Some(NextDialogue::Next(Self::AfterQuestion)),
                    ),
                ],
            ),
            Self::AfterQuestion => DialogueData::new(
                vec![
                    ("I know... I know...", Some("Nicholas")),
                    ("I just -", Some("Nicholas")),
                    ("Oof.", Some("Nicholas")),
                    ("I'm scared, okay?", Some("Nicholas")),
                    ("And I don't like these people.", Some("Nicholas")),
                    ("Just, don't leave me for *too* long.", Some("Nicholas")),
                    ("Please don't.", Some("Nicholas")),
                ],
                vec![("Return", Some(NextDialogue::End))],
            ),
        }
    }
}

fn handle_initial(
    mut option_evs: EventReader<ChosenEvent<Initial>>,
    mut conversation: Query<&mut ConversationIndex, With<Nicholas>>
) {
    let mut conversation = conversation.get_single_mut().unwrap();

    for ev in option_evs.iter() {
        if matches!(ev.current, Initial::AfterQuestion) {
            conversation.0 += 1;
        }
    }
}

#[derive(Clone)]
pub(crate) enum RepeatResponse {
    Start,
}

impl Default for RepeatResponse {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for RepeatResponse {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![("Can you stay around just a little longer?", Some("Nicholas")),
                     ("I could really use someone sane to talk to.", Some("Nicholas"))],
                vec![("Return", Some(NextDialogue::End))],
            ),
        }
    }
}
