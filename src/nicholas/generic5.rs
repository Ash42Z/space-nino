use crate::dialogue::DialogueData;
use crate::gamestate::ConversationIndex;
use crate::topic::{ChosenEvent, NextDialogue, Topic, TopicPlugin};

use bevy::prelude::*;

use super::Nicholas;

pub struct GenericPlugin;

impl Plugin for GenericPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(TopicPlugin::<Initial>::default())
            .add_system(handle_initial)
            .add_plugin(TopicPlugin::<RepeatResponse>::default());
    }
}

#[derive(Clone)]
pub enum Initial {
    Start
}

impl Default for Initial {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for Initial {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![
                    ("Do you think we'll end up getting out alive?", Some("Nicholas")),
                    ("I... I dunno.", Some("Nicholas")),
                    ("As the days go on, I'm beginning to doubt even you.", Some("Nicholas")),
                    ("I wish I could help...", Some("Nicholas")),
                    ("But I'm not good with science stuff.", Some("Nicholas")),
                    ("I'd just clutz it up, I'm sure", Some("Nicholas"))
                ],
                vec![
                    ("[Return]", Some(NextDialogue::End))
                ],
            )
        }
    }
}

fn handle_initial(
    mut option_evs: EventReader<ChosenEvent<Initial>>,
    mut conversation: Query<&mut ConversationIndex, With<Nicholas>>
) {
    let mut conversation = conversation.get_single_mut().unwrap();

    for ev in option_evs.iter() {
        if matches!(ev.current, Initial::Start) {
            conversation.0 += 1;
        }
    }
}

#[derive(Clone)]
pub enum RepeatResponse {
    Start,
}

impl Default for RepeatResponse {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for RepeatResponse {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![
                    ("If you ever need anything done, let me know!", Some("Nicholas")),
                    ("As long as it doesn't have to do with technology.", Some("Nicholas")),
                    ("Or space.", Some("Nicholas")),
                    ("...", Some("Nicholas")),
                    ("Actually, let me know either way.", Some("Nicholas")),
                    ("I can't help, but I'd love to just tag around.", Some("Nicholas")),
                    ("Please.", Some("Nicholas")),
                ],
                vec![
                    ("[Return]", Some(NextDialogue::End))
                ],
            ),
        }
    }
}
