use crate::dialogue::DialogueData;
use crate::gamestate::ConversationIndex;
use crate::topic::{ChosenEvent, NextDialogue, Topic, TopicPlugin};

use bevy::prelude::*;

use super::Nicholas;

pub struct GenericPlugin;

impl Plugin for GenericPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(TopicPlugin::<Initial>::default())
            .add_system(handle_initial)
            .add_plugin(TopicPlugin::<RepeatResponse>::default());
    }
}

#[derive(Clone)]
pub enum Initial {
    Start
}

impl Default for Initial {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for Initial {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![
                    ("I've been rewatching my one of my favourite movies.", Some("Nicholas")),
                    ("Night of the Living Dead.", Some("Nicholas")),
                    ("Remember why I love it so much?", Some("Nicholas")),
                    ("It's a terrific work of art.", Some("Nicholas")),
                    ("Absolutely revolutionized the horror genre.", Some("Nicholas")),
                    ("...", Some("Nicholas")),
                    ("Yeah, so I've been watching it a lot.", Some("Nicholas")),
                    ("...", Some("Nicholas")),
                    ("I guess I just want all this to go away.", Some("Nicholas")),
                    ("It's nice to throw yourself into a different movie, once in a while.", Some("Nicholas")),
                    ("...", Some("Nicholas")),
                    ("My phone broke last week.", Some("Nicholas")),
                    ("So I can't watch it anymore.", Some("Nicholas")),
                    ("Which absolutely sucks.", Some("Nicholas")),
                ],
                vec![
                    ("[Return]", Some(NextDialogue::End))
                ],
            )
        }
    }
}

fn handle_initial(
    mut option_evs: EventReader<ChosenEvent<Initial>>,
    mut conversation: Query<&mut ConversationIndex, With<Nicholas>>
) {
    let mut conversation = conversation.get_single_mut().unwrap();

    for ev in option_evs.iter() {
        if matches!(ev.current, Initial::Start) {
            conversation.0 += 1;
        }
    }
}

#[derive(Clone)]
pub enum RepeatResponse {
    Start,
}

impl Default for RepeatResponse {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for RepeatResponse {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![
                    ("Got any other movies I can watch?", Some("Nicholas")),
                    ("Preferrably, something you have on your phone.", Some("Nicholas")),
                    ("We could watch something together, if you'd like.", Some("Nicholas")),
                ],
                vec![
                    ("[Return]", Some(NextDialogue::End))
                ],
            ),
        }
    }
}
