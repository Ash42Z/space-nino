use crate::dialogue::DialogueData;
use crate::gamestate::ConversationIndex;
use crate::topic::{ChosenEvent, NextDialogue, Topic, TopicPlugin};

use bevy::prelude::*;

use super::Nicholas;

pub struct GenericPlugin;

impl Plugin for GenericPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(TopicPlugin::<Initial>::default())
            .add_system(handle_initial)
            .add_plugin(TopicPlugin::<RepeatResponse>::default());
    }
}

#[derive(Clone)]
pub enum Initial {
    Start
}

impl Default for Initial {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for Initial {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![
                    ("The past month I've had a recurring nightmare.", Some("Nicholas")),
                    ("It's past midnight. I'm standing on an empty street.", Some("Nicholas")),
                    ("There are no cars. There are no people.", Some("Nicholas")),
                    ("I'm all alone, save the light of a lone lamppost.", Some("Nicholas")),
                    ("I can't move.", Some("Nicholas")),
                    ("I'm stuck in place.", Some("Nicholas")),
                    ("Then the lamppost flickers off.", Some("Nicholas")),
                    ("I stay still, the pressure of the darkness slowly overcoming me.", Some("Nicholas")),
                    ("Then I wake up.", Some("Nicholas")),
                    ("I keep telling myself.", Some("Nicholas")),
                    ("...", Some("Nicholas")),
                    ("It's just a dream, Nicholas.", Some("Nicholas")),
                    ("It's just a dream.", Some("Nicholas"))
                ],
                vec![
                    ("[Return]", Some(NextDialogue::End))
                ],
            )
        }
    }
}

fn handle_initial(
    mut option_evs: EventReader<ChosenEvent<Initial>>,
    mut conversation: Query<&mut ConversationIndex, With<Nicholas>>
) {
    let mut conversation = conversation.get_single_mut().unwrap();

    for ev in option_evs.iter() {
        if matches!(ev.current, Initial::Start) {
            conversation.0 += 1;
        }
    }
}

#[derive(Clone)]
pub enum RepeatResponse {
    Start,
}

impl Default for RepeatResponse {
    fn default() -> Self {
        Self::Start
    }
}

impl Topic for RepeatResponse {
    fn data(&self) -> DialogueData<Self> {
        match self {
            Self::Start => DialogueData::new(
                vec![
                    ("I keep telling myself, it's just a dream.", Some("Nicholas")),
                ],
                vec![
                    ("[Return]", Some(NextDialogue::End))
                ],
            ),
        }
    }
}
