use crate::time::AgePhase;

use bevy::prelude::*;

use std::marker::PhantomData;

pub struct GameStatePlugin;

impl Plugin for GameStatePlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(setup_resources)
            .add_system(match_images_to_phase);
    }
}

#[derive(Bundle, Default)]
pub struct CharacterBundle {
    pub character: Character,
    pub name: CharacterName,
    pub trust: Trust,
    pub image: Handle<Image>,
    pub face: Face,
}

#[derive(Component, Default)]
pub struct Character;

#[derive(Component, Default)]
pub struct CharacterName(pub String);

impl CharacterName {
    pub fn new(name: &'static str) -> Self {
        Self(name.into())
    }
}

#[derive(Component, Default)]
pub struct Face(pub Handle<Image>);

pub struct RequestTopicEvent<C> {
    _phantom: PhantomData<C>,
}

impl<C> Default for RequestTopicEvent<C> {
    fn default() -> Self {
        Self {
            _phantom: Default::default(),
        }
    }
}

#[derive(Component, Default)]
pub struct Trust(pub f64);

#[derive(Component)]
pub struct Alive;

#[derive(Component)]
pub struct ConversationIndex(pub u32);

#[derive(Resource)]
pub struct Fuel(pub u32);

#[derive(Resource)]
pub struct NavData(pub u32);

impl NavData {
    pub fn format(&self) -> &'static str {
        let half_range = (MAX_NAV - MIN_NAV) as f32 / 2.;

        if self.0 == 0 {
            "Insufficient"
        } else if self.0 < MIN_NAV {
            "Minimal"
        } else if self.0 < MIN_NAV + (half_range as u32) {
            "Imprecise"
        } else if self.0 < MAX_NAV {
            "Precise"
        } else {
            "Maximum Precision"
        }
    }
}

pub fn minimum_fuel(nav: u32) -> u32 {
    std::cmp::max(
        (MAX_NAV as i32) - (MAX_MIN_FUEL as i32) - (nav as i32),
        MAX_MIN_FUEL as i32,
    ) as u32
}

const MIN_NAV: u32 = 5;
const MAX_NAV: u32 = 15;
const MAX_MIN_FUEL: u32 = 5;

pub fn success_rate(fuel: u32, nav: u32) -> u32 {
    let base: i32 = (fuel + nav) as i32;
    let p = (base - (MIN_NAV as i32) - (minimum_fuel(nav) as i32)) as f64 / 40.;
    let percent: i32 = (p * 100.) as i32;

    if percent < 0 {
        0
    } else if percent > 100 {
        100
    } else {
        percent as u32
    }
}

#[derive(Resource)]
pub struct MissionCount(pub u32);

#[derive(PartialEq)]
pub enum SidequestState {
    NotGiven,
    Given,
    Finished
}

impl Default for SidequestState {
    fn default() -> Self {
        Self::NotGiven
    }
}

fn setup_resources(mut commands: Commands) {
    commands.insert_resource(Fuel(0));
    commands.insert_resource(NavData(0));
    commands.insert_resource(MissionCount(0));
}

fn match_images_to_phase(
    phase: Res<AgePhase>,
    asset_server: Res<AssetServer>,
    mut images: Query<(&CharacterName, &mut Handle<Image>, &mut Face)>,
) {
    if !phase.is_changed() {
        return;
    }

    let suffix = match *phase {
        AgePhase::Start => {
            return;
        }
        AgePhase::Middle => 2,
        AgePhase::End => 3,
    };

    for (name, mut image, mut face) in images.iter_mut() {
        *image = asset_server.load(format!("{}{}.png", name.0, suffix));
        face.0 = asset_server.load(format!("{}Face{}.png", name.0, suffix));
    }
}
