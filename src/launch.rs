use crate::gamestate::{success_rate, Fuel, NavData};
use crate::utils::{
    handle_change_state_button, spawn_button_with_component, ChangeStateButton,
};
use crate::{ButtonImage, GameFont, GameView, Ui, UiBox};

use bevy::prelude::*;
use iyes_loopless::prelude::*;

pub struct LaunchPlugin;

impl Plugin for LaunchPlugin {
    fn build(&self, app: &mut App) {
        app.add_enter_system(GameView::Launch, load_launch)
            .add_system(handle_change_state_button);
    }
}

const VISIBLE_OPTIONS: f32 = 2.;

fn load_launch(
    mut commands: Commands,
    font: Res<GameFont>,
    button_image: Res<ButtonImage>,
    ui: Query<Entity, With<Ui>>,
    box_style: Query<&Style, With<UiBox>>,
    fuel: Res<Fuel>,
    nav: Res<NavData>,
) {
    let Ok(ui) = ui.get_single() else { return; } ;
    commands.entity(ui).despawn_descendants();

    let Ok(Val::Px(width)) = box_style
        .get_single()
        .map(|style| style.size.width)
        else { return; };

    let flex_box = commands
        .spawn(NodeBundle {
            style: Style {
                size: Size {
                    height: Val::Px(width),
                    ..default()
                },
                display: Display::Flex,
                flex_direction: FlexDirection::Column,
                justify_content: JustifyContent::SpaceBetween,
                ..default()
            },
            ..default()
        })
        .id();

    let text_box = commands
        .spawn(NodeBundle {
            style: Style {
                flex_direction: FlexDirection::Row,
                align_items: AlignItems::FlexStart,
                ..default()
            },
            ..default()
        })
        .id();

    let button_box = commands
        .spawn(NodeBundle {
            style: Style {
                flex_direction: FlexDirection::Column,
                size: Size {
                    height: Val::Px(VISIBLE_OPTIONS * 0.125 * width),
                    ..default()
                },
                ..default()
            },
            ..default()
        })
        .id();

    commands
        .entity(flex_box)
        .push_children(&vec![text_box, button_box]);

    let text = commands
        .spawn(TextBundle {
            text: Text::from_section(
                format!(
                    "Traveling back to earth safely will succeed \
                          at {}%. Are you sure?",
                    success_rate(fuel.0, nav.0)
                ),
                TextStyle {
                    color: Color::AZURE,
                    font: font.0.clone(),
                    font_size: width * 0.08,
                },
            ),
            style: Style {
                max_size: Size {
                    width: Val::Px(width) * 0.96,
                    ..default()
                },
                ..default()
            },
            ..default()
        })
        .id();

    commands.entity(text_box).add_child(text);

    let children = vec![
        spawn_button_with_component(
            &mut commands,
            "Launch".into(),
            &font.0,
            &button_image.0,
            width,
            ChangeStateButton(GameView::End),
        ),
        spawn_button_with_component(
            &mut commands,
            "Back".into(),
            &font.0,
            &button_image.0,
            width,
            ChangeStateButton(GameView::Cockpit),
        ),
    ];
    commands.entity(button_box).push_children(&children);
    commands.entity(ui).add_child(flex_box);
}
