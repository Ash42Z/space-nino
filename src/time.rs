use crate::{GameFont, GameView, UiBox};

use bevy::prelude::*;
use bevy::time::Stopwatch;
use chrono::{
    Datelike, Duration as ChronoDuration, NaiveDate, NaiveDateTime, Timelike,
};
use iyes_loopless::prelude::*;

#[derive(Resource)]
pub struct StationTime {
    watch: Stopwatch,
    pub multiplier: u32,
}

fn base_date() -> NaiveDateTime {
    NaiveDate::from_ymd_opt(2117, 12, 3)
        .unwrap()
        .and_hms_opt(14, 0, 0)
        .unwrap()
}

pub struct TimePlugin;

impl Plugin for TimePlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(setup_resource)
            .add_system(tick_time)
            .add_system(death_system);

        app.add_system_set(
            ConditionSet::new()
                .run_if(should_timer_tick)
                .with_system(update_time_text)
                .into(),
        );

        app.add_enter_system(GameView::Spacestation, load_time_ui)
            .add_enter_system(GameView::Spacestation, start_time);
    }
}

#[derive(Resource)]
pub struct DisplayTimeInfo(pub bool);

#[derive(Resource, PartialEq)]
pub enum AgePhase {
    Start,
    Middle,
    End,
}

impl AgePhase {
    fn did_transition(&self, time: ChronoDuration) -> Option<Self> {
        match self {
            Self::Start => {
                if time > ChronoDuration::weeks(52 * 15) {
                    return Some(Self::Middle);
                }
            }
            Self::Middle => {
                if time > ChronoDuration::weeks(52 * 30) {
                    return Some(Self::End);
                }
            }
            _ => {}
        }

        None
    }
}

fn should_timer_tick(state: Res<CurrentState<GameView>>) -> bool {
    !matches!(state.0, GameView::TitleScreen | GameView::Intro)
}

fn setup_resource(mut commands: Commands) {
    let mut watch = Stopwatch::new();
    watch.pause();

    let time = StationTime {
        watch,
        multiplier: 1,
    };

    // Time is a resource ^.^
    commands.insert_resource(time);
    commands.insert_resource(AgePhase::Start);
    commands.insert_resource(DisplayTimeInfo(false));
}

fn tick_time(
    mut station_time: ResMut<StationTime>,
    time: Res<Time>,
    mut phase: ResMut<AgePhase>,
) {
    add_seconds(&mut station_time, time.delta().as_secs_f64());

    if let Some(next_phase) = phase.did_transition(
        ChronoDuration::from_std(station_time.watch.elapsed()).unwrap(),
    ) {
        *phase = next_phase;
    }
}

pub fn add_seconds(station_time: &mut StationTime, seconds: f64) {
    let d = std::time::Duration::from_secs_f64(
        seconds * station_time.multiplier as f64,
    );

    station_time.watch.tick(d);
}

#[derive(Component)]
pub struct TimeUi;

#[derive(Component)]
struct TimeText;

const MONTH_NAMES: [&'static str; 12] = [
    "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct",
    "Nov", "Dec",
];

pub fn format_seconds(seconds: u32) -> String {
    let dur = ChronoDuration::seconds(seconds as i64);

    if dur.num_weeks() > 0 {
        format!("{} weeks", dur.num_weeks())
    } else if dur.num_days() > 0 {
        format!("{} days", dur.num_days())
    } else if dur.num_hours() > 0 {
        format!("{} hours", dur.num_hours())
    } else if dur.num_minutes() > 0 {
        format!("{} minutes", dur.num_minutes())
    } else {
        format!("{} seconds", dur.num_seconds())
    }
}

fn update_time_text(
    mut text: Query<&mut Text, With<TimeText>>,
    time: Res<StationTime>,
) {
    if let Ok(mut text) = text.get_single_mut() {
        let date = base_date()
            + ChronoDuration::from_std(time.watch.elapsed()).unwrap();
        text.sections[0].value = format!(
            "{}, {} {}, {:02}:{:02}:{:02} ({} / sec)",
            date.year(),
            MONTH_NAMES[date.month0() as usize],
            date.day(),
            date.hour(),
            date.minute(),
            date.second(),
            format_seconds(time.multiplier)
        );
    }
}

fn start_time(mut time: ResMut<StationTime>) {
    time.watch.unpause();
}

fn load_time_ui(
    mut commands: Commands,
    font: Res<GameFont>,
    ui: Query<Entity, With<TimeUi>>,
    box_style: Query<&Style, With<UiBox>>,
    display_time: Res<DisplayTimeInfo>,
) {
    if !display_time.is_changed() || !display_time.0 {
        return;
    }

    let Ok(ui) = ui.get_single() else {return;};
    commands.entity(ui).despawn_descendants();

    let Ok(Val::Px(width)) = box_style
        .get_single()
        .map(|style| style.size.width)
        else { return; };

    let text = commands
        .spawn((
            TimeText,
            TextBundle {
                text: Text::from_section(
                    "0",
                    TextStyle {
                        color: Color::ANTIQUE_WHITE,
                        font: font.0.clone(),
                        font_size: width * 0.05,
                    },
                ),
                ..default()
            },
        ))
        .id();

    commands.entity(ui).add_child(text);
}

fn death_system(mut commands: Commands, time: Res<StationTime>) {
    if ChronoDuration::from_std(time.watch.elapsed()).unwrap()
        > ChronoDuration::weeks(52 * 60)
    {
        commands.insert_resource(NextState(GameView::Death));
    }
}
