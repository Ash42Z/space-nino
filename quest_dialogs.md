# Nicholas's Food
Hey
I haven't seen you for _soooo long_
_Too long_
How was your last journey?
* Great, I didn't think it could go any better
* Good, maybe I could do better but I'm happy with the results
// I'm glad to hear
* Not so good, I really hoped that I will get more out of it
* Really bad, I feel it was just a waist of time
// I'm sorry to hear
Listen
I have so much to tell you
Do you remember the really speciel canned meat that I brought with us to the ship?
* No
* Not really
* Canned meat?
Never mind
So a few weeks ago I wanted to cook it
And it wasn't there
Someone actually ate it
WHO EATS SOMEONE ELSE'S CANNED MEAT?!?!?!
I have my suspects but I would like to hear your opinion too
Who do you think ate it?
* Patricia
// Of course, how didn't I think of her before
// She is too smart, of course she knows the true value of a good canned meat
// I'm sure she took it 
* Nora
// It must be Nora!
// She is always stress eating
// I sometimes fear she will finish all the food in this ship
// She probably ate my canned meat too
* Definitely Stanley
// Thats what I thought too!
// He doesn't care about anyone here
// Why would he be consider of other's food???
* I don't feel like taking side
// Really?!?!
// You're defending everyone else?
// And I thought you were my friend...
Well 
It was nice talking to you again
Come back to the ship more often!
I'll be waiting for you!


# Stanley's Illness
[Stanley]
Cough Cough
* Are you okay?
* You look a little green
I'm not feeling well
I think this is going to be my end
* What's wrong?
* How do you feel?
I'm really weak and my head spinning
I can barely walk to the bathroom
And I can barely eat
Its all your fault you know
I'll be dead soon and it will be on YOUR CONSCIENCE
[Patricia]
It just a simple virus, I can probably help you
[Stanley]
Really?
[Patricia]
I just need some ?adamantium? and I could make a medicine in no time
* Do we have some on this ship?
// Unfortunately no
// But its pretty common in space. You can probably find that in a close space rock
* Where can we get that?
// Its pretty common in space. You can probably find that in a close space rock
// The ship's computer should be able to detect it in no time
[Nora]
So it requires another mission?
We don't have time for that!
This old geezer is going to die on this ship anyways.
Can we focus on getting this ship back to earth?
* It will be just a little detour, it wouldn't take long.
// I really hope you are right
// Time is for the essence, you know
// If everyone here will die it will be on your conscience too
* I agree. Repairing this ship is the top priority.
// Thanks
// I know that its a difficult choice but its the correct one
// We should focus on the big picture in those situations - saving this ship and getting back home quickly
* I need more time to think about this.
// I understand, its a difficult situation
// Be carefull not to take the wrong choice
// It's this entire ship's lifes, not just stanley's, you know

# Patricia's First Science Pastenino
[After the first recommendation journey]
Hey you
Isn't space wonderfull?
There is so many thing to research around here
And the time to do it, too!
By the way, how was your last journey? Was my recommendation usefull?
* Of course! This journey was really successfull and I got so much data!
// I'm not suprised
// The interference between the cosmic radiation and the gravitational waves can really help with callibrating navigation systems.
* It was fine. I didn't think that the recommendation changed a lot though.
// Interesting.
// Maybe the solar burst changed the interference pattern and harm the callibration.
* It was awful. Your recommendation didn't help me at all! 
// Weird
// Maybe I had an error in my calculation
// I'll check it later
Anyways
I had another idea for a good journey
A REALLY good journey
Are you ready?
* Whats your idea?
* Talk to me
So
In 2098 prof. Laybre came up with a theory that sometimes space rocks near a black hole can be made entirely of
 anti-dark-matter.
Its really difficult to measure that so in years no one was able to test this theory.
Well
Until now!
I've been able to find a rock which contains that!
And it will soon be reachable by our pod!!
And you can go to bring it here!!!
Its really speciel!!!!!
Are you exited too???
* WOW its sounds amazing!!!
// I know, right?
* Why is it important?
// For science, of course
* Anti-dark-what?
// Never mind
// It just a really important discovery
I'll program the coordinates to your computer
Make sure not to miss this oportunity!

# Patricia's Second Science Pastenino
[Only if the first pastenino was done]
*Whitles*
Oh hey, I didn't notice you there
I'm deep into my research lately
Its not like there's anything else to do here, you know...
...
......
Anyhow
I had another really good idea for a research journey
Are you ready?
* Go for it
// Great!
* Another one???
// Don't be like this...
// Just listen
So
I was wondering about science during your last journey
You know
Like I always do
And then I asked myself - could there be life in space that we haven't considered yet?
Maybe life in some other form than the ones we usually think about
So i tried digging into supernova's remnants
And I found out that really close to us there used to be a planet
Well
More like a white dwarf
Which contained a lot of water
In a gas form of obviously
And when it collapsed into the black hole next to us it also spread some its water vapor in a way which made them to move in an orbit around the black hole
This vapor cooled down with time, turned to ice and trapped some of the supernova remnant inside
And guess what?
We have a really good opportunity to bring some of it back for some research!!!!
What do you say?
* Sound really important! I'm in!
// Wonderfull!
* Isn't it just waisting our time a little?
// We have enough time to fix the ship later
// This big discovery happens one in a millennium!
* Supernova what?
// You'll understand when you'll see it
I've already entered the destination to your computer so you don't miss it.
Good luck!

# Patricia's Last Science Pastenino
[Only if the second pastenino was done]
You!!!
I have an amazing news!!!!
Do you remember your last two science journeys?
