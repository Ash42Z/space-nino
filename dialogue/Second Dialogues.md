[First time you press the missions button]
[Nicholas] "Where are you going?"
* [explain how you're going to fix the ship]
* To fix the ship
[Nora] "Hurry up then."

[After The First Mission (~a month/year passed for them, only a few minutes for you)]
[You get back from the mission]
[Nora] "YOU!"
[Nicholas] "I knew you would come back!"
* What's all this ruckus about?
* I was just gone for [short time that happened in the game].
[Nora] All this ruckus?
[Nora] ALL THIS RUCKUS???
//
[Nora] Just a [time]?
[Nora] JUST A [time]???
//
[Nora] YOU WERE GONE FOR A [long time]!!
[Patricia] Oh, of course!
[Nora] What?
[Patricia] It's so obvious!
[Nicholas] What?
[Patricia] How did we not see this before?
[Stanley] SPIT IT OUT ALREADY
[Patricia] It's the Black Hole!
* What.
* What?
[Patricia] Time works differently around Black Holes.
[Patricia] The close you are to the Black Hole,
[Patricia] The slower time moves for you,
[Patricia] And the faster it goes for us.
[Patricia] If you go down below. Even for a minute!
[Patricia] We might have to wait years to see you come back.
[Patricia] Years!
[Nora] YEARS?!!
[Stanley] Ugh...
[Patricia] It's trivial if you know a bit of General Relativity.
[Stanley] Then it's lucky I don't.
[Nora] What does this mean for us?
[Patricia] It means... We have less time than we thought.
[Stanley] I knew this trip would be the death of me.
[Stanley] I knew it.

[Patricia]
I can't believe I didn't think of it before.
This is fantastic!
Experiencing differences in time dilation.
Who knew we'd live to see the day?
* [generic choice]
When you gather data, do it carefully.
Take your time.
The only way we survive is if we gather accurate data,
and as much of it as possible.
[something about giving tips in the future]

[Patricia]
Oh, did you come back for some tips?
You should gather navigation data in [bad nav data mission location]
Its clearly the best research opportunity right now.

[Nora]
I have a family, you know.
My son is waiting for me at home.
He's only eleven.
What will he do without me?
I don't care about all that physics nonsense.
Just fix the ship quick as you can.
I have to get back.
I have to.

[Nora]
How long will it take to fix this ship?
I must get back home soon...

[Stanley]
What?
Take all the time you want.
Not like I care.
I never thought I had a chance to live anyway.
...
If you're planning on leaving again for a long time,
Maybe don't come back.

[Stanley]
I don't want to talk to you.
Could you stop looking at me?

[Nicholas]
You were gone for so long!
I don't want to be alone with these strangers.
Do you have any idea how difficult the past month was for me?
Ugh, with Stanley.
He's so grumpy.
I can barely stand for us to be in the same room.
And Nora.
She's so _annoying_.
All she talks about is her son.
And Patricia!
Well, I guess she's okay.
But who knows what I'll think of her in a month.
Or a year!
That's too much for me, you know?
...
You're my best friend.
Please, don't leave me alone again.
* I'm sorry... You know there's no other way, I have to fix the ship.
* What else am I gonna do?
I know... I know...
I just -
Oof.
I'm scared, okay?
And I don't like these people.
Just, don't leave me for _too_ long.
Please don't.

[Nicholas]
Can you stay around just a little longer?
I could really use someone sane to talk to.
