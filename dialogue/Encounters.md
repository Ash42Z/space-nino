* Asteroids on the way down.
  * If you decide [to be careful in descent], it will take more time, but you will definitely reach the place.
  * If you decide [to rush it], it will take less time, but you only have a ~70% chance of succeeding.
  * Or you could decide [to go back] (which will take much less time), but you definitely fail.
* Sudden radiation burst makes it hard for you to tell where you're trying to get to
  * You could go back up (out of scope of the burst)
  * Or take your chances in reaching your objective
* About to land on an asteroid, you notice the asteroid looks strange from afar (maybe a bit moldy, maybe radioactive) and you can decide whether to go get fuel from there anyway.
  * You can go back, which means you don't get a fuel cell.
  * You can get the fuel anyway (but later mold that comes back up causes Stanley to fall ill)
* The ship has a radio, but only short-range. You go down with your pod, and suddenly receive a signal saying "SOS".
  * You can decide to follow the signal, but it turns out that it is only your crewmates who sent the signal, and you've wasted a mission.
* You reach the asteroid, but turns out the fuel material is deep inside the asteroid, and it will take [extra time] to get the fuel properly.
  * Time/fuel tradeoff
* In your mind you suddenly hear sounds "Mmmm... I am an alien."
  * If you follow the sounds you find an alien.
  * The alien eats some of your fuel. "Yumum yum yum."
* On the way down, you receive a call from the space ship that the fuel is leaking.
  * If you decide to return and fix the space ship immediately: You don't lose fuel but forgo the mission.
  * If you decide to help afterwards: You succeed the mission but lose some fuel.
  * Maybe Patricia can fix it (but won't agree unless your Patricia stat is high enough) (and if you go up anyway she thinks you trust her less)
  * (A similar tradeoff could exist in a mission where the problem is on _your_ pod)
* On the way down, suddenly you lose all visuals. If you go out to check what happened, you find a space sheep.
  * It's a space sheep.
  * "Space baa"
* On the way down you get a call from Nicholas. "Come back! Nora is trying to kill me!"
  * If you go back up, nothing happens. Nora never tried to kill anyone, Nicholas just wanted to see you.
* On one of the short missions, you go down, and stop to think. You realize you haven't had time for yourself in a long time. The others only see you once in a while, but it's been nonstop work for you. Should you maybe take some time off to be more sane?
  * Possibly take time off and wait before going back up. (Doesn't give you anything in return.)
* If you go close enough to the black hole, you hear a voice calling you from inside, beckoning for you to pass the event horizon.
  * If you agree, you (at least think that) you pass through, then you have some weird hallucination-like experience, see light, and then you appear beside the ship again.
* You see light. Do you go to it?
  * Turns out it's a mirror.
* You find another space ship - and if you go check, you find it's deserted - but there is some instant cake/other types of food that you can bring back.
