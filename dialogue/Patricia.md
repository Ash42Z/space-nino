# 1
Me?
I'm fine
More than fine
This ship is a really good opportunity to work on my research
No one was able to stay such a long time next to a black hole
How can I be disapointed?
I really hope that my discoveries would be able to reach earth though.
[Loop]


# 2
Did you knew that in earth I use to be an astrophysicist?
My research was about the behavior of small particles near the event horizon of black holes.
Actually, this is why I wanted to go on this trip.
It's very different to see a black hole when you understand all the physical phenomenons.
If you want I'll explain more about them some day.
[Loop]
Did you come to hear some explanations?
* Maybe later
// Ok
// Let me know when you do.
* Sure
// Great!
// Can you see the light ring around the black hole?
// Its made of [some unclear scientific term]
// The light is created because the [some other science jibber jabber]
// *Patricia drifts of the topic and teach you some random scientific stuff that you doesn't understand*
// *At some point you just walk away and Patricia just continues to talk to herself*

# 3
I really like it here, you know
Something about this endless silence really makes you think about your life
Like "Why did I chose this path in life?"
Or "For what reason am I living?"
"Did life was created if the physical rules of the world were a little different?"
And "Is this life real or are we just living in a simulation?"
"And if so, can we run the same simulation?"
"And in this simulation does the people will run the simulation again?"
I wonder if the others think about those things too...
[Loop]
I wonder what would you think about if you were stuck with us for years too

# 4
Hey
Did you heard about my new discovery?
I found a new semi-particle that plays part in a light-matter interactions close to black holes!!!
Isn't it exiting?
* This is amazing!
* Maybe if I understood it...
There are so many things to learn here!
[Loop]
Do you need me?
* Not really
So I'll go back to my research.
If you do need me, let me know

# 5
Space is so interesting
I'm able to discover SO MANY new things here!!!!
I sometimes wonder if we should even get back to earth at all
...
......
.........
Well, it just a thought
[Loop]
I hope you didn't took it too seriously.
But if you did, let me know.

# 6
