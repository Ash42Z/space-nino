## The Scientist (Patricia)

- "All is not lost."
- "We just need to be patient."
- "No such thing as too much research."
- "Now's not the time to rush it."

## The Married (Nora)

* "There's no time to dally."

## The Old (Stanley)

* "I'm a dead man."
* "Recklessness is what this is."
* "Not like I have long to live anyway."
* "Of course I blame you. Who else?"
* "This ship just became one big coffin."
* "To think _this_ will be my grave..."
* "Easy for you to say."
* "What do you want from me? Leave me alone."
* "Haven't you done enough?"
* "At least I will meet my mother soon"

## General

* Interactions between you and the characters
* Interactions between the characters
  * Conflicts
* Game State (Affects Story State)
  * Time
  * Resources
* Story State
  * Character State
    * each character has their own relevant stat
  * Character Relationships
  * (game/story state history)

* Visible stats are trinary (low / medium / high). Behind the scenes might be continuous.

* (pod <---> lounge <---> pilot room)