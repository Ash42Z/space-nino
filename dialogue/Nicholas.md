# 1
I found an old sewing kit in one of the drawers.
So now I'm trying out knitting.
The first few tries hurt,
but no pain no gain, right?

# 2
Remember that time we pranked the principal?
Haha, yeah, with the spaghetti.
The shock on his face when he opened his desk.
Those were the good old days.

# 3
Out of all the ways I thought I would die
A black hole was not one of them.
It's very pretty.

# 4
Do you think we'll end up getting out of this alive?
I... I don't know.
As the days go on, I'm beginning to doubt it.
I wish I could help, but...
You know how I am with technology.
I'd just clutz it up, I'm sure.

# 5
The past month I've had a recurring nightmare.
It's past midnight. I'm standing on an empty street.
There are no cars. There are no people.
I'm all alone, save the light of a single lamppost.
I can't move.
I'm stuck standing in place.
Then the lamppost flickers off.
I stay still, the pressure of the darkness slowly overcoming me.
Then I wake up.
...
I keep telling myself.
It's just a dream, Nicholas.
It's just a dream.

[LoopLater]
I keep telling myself, it's just a dream.

# 6
I've been rewatching my all-time favourite movie.
Night of the Living Dead.
Remember why I love it so much?
It's a work of art.
Absolutely revolutionized the horror genre.
...
I guess I just want all this to go away.
It's nice to throw yourself into a different world, once in a while.
...
My phone broke last week.
So I can't watch it anymore
which absolutely sucks.

[LoopLater]
Got any other movies I can watch?
On your phone, preferrably.

# 7
I'm running out of ways to keep myself busy.
Please get us out soon.
I think I'm going insane.

# 8
I've been having a bad day.
Just - leave me be.
I need to rest.

# 9
I keep thinking of why I came here in the first place.
A free trip through space.
Was it worth it?
...
Well, was it?

# 10
You have pretty good timing, you know.
Know what day it is today?
* Uh
* Um
* ...yes?
Ugh, it's my birthday. Again.
I'm turning 30.
It's been ten freaking years on this ship.
I'd ask you to celebrate with me, but it's not like you're staying for long.
What do you do on all those trips anyway?
You've been on quite a few, and I don't feel any closer to going back home.
I dunno.
Maybe I'm going a little crazy.
It's just -
Ugh.
This sucks.
* Yeah, it sucks.
// ...
// Oh well.
* Happy birthday.
// Gee, thanks.
I'm going back to my room soon.
Best I can do to celebrate is be away from all these people.
Time alone is a precious resource these days.
