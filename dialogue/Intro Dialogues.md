[You see a spaceship from outside]
"What's that out the window?"
[Asteroid hits the ship]
[[Screen jumps to black]
"Oww..."
"What happened?"
"Did something hit us? Are we dead?"
[Lights flicker on from inside, you see everyone]

"What happened?"
* We ran out of fuel
* The engines were fried
* We lost all data as to where Earth is
"Oh, that's pretty bad."
* And the engines were fried
* And we lost all data as to where Earth is
"???"
* Also, we lost all data as to where Earth is
"Could it get any worse?"
* Oh, and we are beside a black hole.
[characters begin to panic]
[Patricia] "A black hole?!"
[Stanley] "Are you _trying_ to kill us?!"
[Nicholas] "I'm sure it was out of our control."
[Nora] "How could you let this happen??"
[Stanley] "Is that it? Are we going to die? With _these_ people?"
[Nora] "Hey!"
[Patricia] "That's no way to talk, old man!"
* Everyone, calm down!
* Please, calm down!
* You have nothing to worry about.
? [Nora] "Oh yeah? What are you gonna do about it?"
? [Nicholas] "There must be something you can do."

[Stanley]
What makes you think I'll talk to you?
You just put me in a coffin.
* Come on, it's not _that_ bad.
* Hey, we're all in this together.
// Bad? This is a disaster. I almost had a heart attack.
// Togther? I blame you. Of course I blame you. Who else?
* We'll figure this out.
* Don't worry. I can fix this.
Whatever. I'm a dead man.
It's not like I have long to live anyway.

[Stanley]
What do you want from me?
Leave me alone.

[Stanley]
Haven't you done enough?

[Nora]
"I can't be stuck here forever."
"You better do something about it."
* I will, I will.
* You have nothing to worry about.
"I can't die here."
"My family needs me back at home."

[Nora]
"Don't you dare let me die."

[Nicholas]
"A black hole. Pretty bad luck, huh?"
* You don't know the half of it.
* I'm so sorry I took you on this trip.
I'm not worried. I'm sure you can handle it.
You're the best of the best.

[Nicholas]
If worst comes to worst, at least you tried your best.

[Patricia]
Real talk though.
How are you going to sort this out?
* [science mumbo jumbo]
* [science mumbo jumbo with the phrase "quantum"]
* [just the word "quantum"]
Don't try those tricks around me.
I understand exactly what you're saying. I know it's utter nonsense.
* Honestly, I'm not sure yet.
Well, you better think of something, fast.
I don't know how long we can survive above a black hole.
But it can't be long.
