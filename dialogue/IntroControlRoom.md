[Pilot room]
[Data showing empty fuel]
[Data showing autopilot off]
[TROUBLESHOOT]
* (Button to travel to missions (like button to go to ship lounge))
* (Button to go back home) (unavailable to press at first, but visible)

[When pressing Troubleshoot button]
"COMPUTER STARTING..."
"INITIATING TROUBLESHOOTING PROTOCOL..."
"Good morning! Only. 31588. Problems found."
"Beginning Auto Fixing..."
"Auto Fix Successfull."
"Good morning! Only. THREE. Problems found."
"Fuel Tank: Empty."
"Autopilot: Navigation Data Corrupted"
"Passengers: Four"
"Calculating Solutions..."
"Solutions found. What do you want to know?"
* Filling the Fuel Tank
// The Fuel Tank was broken by the hit, and all the Fuel leaked out.
// AutoFix fixed the Tank, but there is still no Fuel.
// Fuel Cells can be synthesized from Plutonium.
// Nearby Plutonium Asteroids Detected.
* Fixing Autopilot
// Calibration was lost. Position of Earth is unknown.
// Autopilot reset. Navigation Data not found.
// New Calibration is Required.
// Spacial Locations for Possible Calibration Measurements Found.
// Bring Navigation Data Back to Complete Autopilot Initialization.
* Passengers?
// Chances of survival estimated at 0.003%.
* Exit
