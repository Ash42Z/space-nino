# 1
Oh look, you're back. _Again_.
Know when we are going back home?
* Soon.
// Good.
// I don't want to be here any more than I need to.
* Not sure yet.
// Well, then let me know when you are.
// I can't begin to describe how anxious this makes me.
* Still working on it.
// Get on it, then.
// As my nana used to say:
// There's no such thing as working too hard.
// Only not enough.

# 2
Did I tell you about my son?
His name is Joshua, and he has the sweetest soul you'll ever meet.
Once, he woke up in the middle of the night, because a butterfly
flew into his room, and landed on his face.
He caught it with his bare hands, carried it outside, and set it free.
The butterfly came back the next week.

# 3
I fix bicycles for a living.
I inherited the shop from my mom. It's been passed down for generations.
I never liked the job, at first. I absolutely hated it.
But after a few years or so... it grew on me.
I guess what I'm trying to say is...
... I like bicycles.
